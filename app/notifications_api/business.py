import jinja2
import os
import settings
from app import mongo, LOG
from bson import ObjectId
from datetime import datetime, timedelta
import json
from app.utils import app_business, notifications


def _set_up_applicant_dispute_added_immediate_notification(applicant_id, alert_text):
    """
    This method add new alert and sends notification to applicant immediately
    Args:
        applicant_id: <ObjectId()> - applicant's ID
        alert_text: <str()> - Alert text to be saved

    Returns: notification id of the new notification object in collection
    """
    applicant = app_business.lookup_user_in_collection(applicant_id, 'applicants')
    context = {'applicant': applicant}
    template_path = settings.TEMPLATES_PATH + 'email_templates/adv_action/applicant/dispute_added.html'
    message = app_business.render(template_path, context)

    dash_alert = {
        'level': 'alert',
        'title': 'Dispute submitted',
        'text': 'The candidate submitted a dispute for {}.'.format(alert_text),
        'status': 'pending',
        'is_active': False,
        'alert_type': 'dispute_submitted',
        'action_present': False,
        }

    rec_settings = notifications.T.rec_settings.daily
    notification_id = app_business.create_notification(
        message_to=applicant['email'],
        message_body=message,
        message_subject="Vetty has received your dispute",
        message_from="vetty@support.vetty.co",
        from_name="Vetty",
        type='dispute_submitted',
        is_active=True,
        rec_settings=rec_settings,
        total_count=1,
        recipient=applicant['_id'],
        status='immediate',
        status_message='initially active',
        when_exhausted={'callback': 'issue_dash_alert_for_recipient_applicant',
                        'params': dash_alert}
    )
    return notification_id


def _set_up_support_dispute_added_immediate_notification(applicant_id, alert_text):
    """
        This method add new alert and sends notification to client immediately
        Args:
            applicant_id: <ObjectId()> - applicant's ID
            alert_text: <str()> - Alert text to be saved

        Returns: notification id of the new notification object in collection
        """
    applicant = app_business.lookup_user_in_collection(applicant_id, 'applicants')
    client = app_business.lookup_user_in_collection(applicant['client_id'], 'clients')

    pre_adv_initiated = True if applicant.get('client_results', dict()).get('client_remarks') == 'pre_adverse' else False
    context = {'applicant': applicant, 'pre_adv_initiated': pre_adv_initiated, 'alert_text': alert_text}
    template_path = settings.TEMPLATES_PATH + 'email_templates/adv_action/vetty/dispute_added_new.html'
    message = app_business.render(template_path, context)

    rec_settings = notifications.T.rec_settings.daily
    notification_id = app_business.create_notification(
        message_to=settings.SUPPORT_EMAIL,
        message_body=message,
        message_subject="Dispute filed: {candidate_name} --- {client_account}".format(candidate_name=applicant['first_name']+' '+applicant['last_name'], client_account=client['client_name']),
        message_from=settings.SUPPORT_EMAIL,
        from_name="Vetty",
        type='dispute_submitted',
        is_active=True,
        rec_settings=rec_settings,
        total_count=1,
        subject_id=applicant['_id'],
        status='immediate',
        status_message='initially active'
    )
    return message


def create_dashboard_notification_for_applicant(applicant_id, notification):
    notification['created_at'] = datetime.now()
    notification['status'] = 'issued'
    notification['status_message'] = "issued by uiservice"
    res = mongo.applicants.update({'_id': applicant_id}, {'$push': {'notifications': notification}})
    return True


def disable_dashboard_alert_of_type_for_applicant(alert_type, applicant_id, reason=""):

    d = mongo.applicants.update_one(
        {
            "_id": applicant_id,
            'alerts': {"$elemMatch": {'alert_type': alert_type, 'is_active': True}}
        },
        {'$set': {
            "alerts.$.is_active": False,
            "alerts.$.status": "deactivated",
            "alerts.$.status_message": reason
        }}
    )
    return d.modified_count == 1
