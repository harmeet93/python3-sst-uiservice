from bson import ObjectId
from app import mongo, LOG
from app.exceptions import NoRecordFound, LoginError, ApplicantUploadNotFound, APIRequestFieldMissing, \
    ApplicantLookupError, ApplicantMissingField
from flask_wtf import FlaskForm, RecaptchaField
from wtforms import PasswordField, StringField
from wtforms.fields.html5 import EmailField
from wtforms import validators
from wtforms.validators import Required
from flask_wtf import recaptcha
from flask import request
from werkzeug.security import check_password_hash, generate_password_hash
from app.utils import app_business
from app.jwt_auth import issue_access_token_for_api_user
from app.utils.applicant import check_code_to_human_name
from app.notifications_api import business as notifications_business
from werkzeug.utils import secure_filename
from datetime import datetime
import settings
import os
from app.sign_pdf import put_png_string_onto_pdf, sign_paintzen_offer_letter, sign_w9_form, sign_mvr_driver_form
from app.sign_pdf import sign_ca_disclosure, sign_bc_authorization, sign_bc_disclosure


def get_uploads_object(applicant_id, upload_type="all"):
    """
        :param applicant_id: <ObjectId()> - applicant's ID
        :param upload_type: <str> default: "all" - specific upload_type or "all"
        :return: list of upload documents satisfying criteria.
        """
    uploads = app_business.list_uploads_for_applicant(applicant_id, upload_type)
    if not uploads:
        raise ApplicantUploadNotFound("Uploads of type '{}' not found.".format(upload_type))
    res = []
    for d in uploads:
        d['_id'] = str(d['_id'])
        d['upload_path'] = '/static' + d['upload_path'].split("static")[1]
        d['upload_url'] = d['upload_path']
        d['owner_id'] = str(d['owner_id'])
        res.append(d)
    return res


def delete_own_upload_applicant(applicant_id, upload_strid):
    """
    This function finds and update the upload object as deleted
    :param applicant_id: <ObjectId()> - applicant's ID
    :param upload_strid: upload's ID as str
    :return: boolean True/False
    :raise: ApplicantUploadNotFound: when uploads object not found
    """
    upload = mongo.uploads.find_one({'_id': ObjectId(upload_strid), 'owner_id': applicant_id})
    if not upload:
        raise ApplicantUploadNotFound("No upload {} registered for applicant {}".format(upload_strid, applicant_id))
    return app_business._mark_upload_deleted_by(upload['_id'], applicant_id, "applicants")


def applicant_login(request):
    json_data = request.get_json(force=True)
    check = app_business.check_protection(json_data)
    if check:
        return False
    return applicant_login_ssn_agnostic(json_data)


def applicant_login_ssn_agnostic(params):
    applicant = app_business.find_applicant_by_email_case_insensitive(params['email'])
    if not applicant:
        LOG.info("Applicant_api v2 login error: {}".format("applicant not found"))
        return False

    api_user = mongo.api_users.find_one({'applicants': applicant['_id']})
    if api_user is None:
        LOG.info("Applicant_api v2 login error: {}".format("api_user not found"))
        return False
    try:
        password_matches = check_password_hash(api_user['password'], params['password'])
        if not password_matches:
            LOG.info("Applicant_api v2 login error: {}".format("password with api_user not found"))
            return False
    except Exception as e:
        return False

    access_token = issue_access_token_for_api_user(api_user)
    return access_token


def get_applicant_data(params):
    try:
        applicant = mongo.applicants.find_one({
                "_id": ObjectId(params['applicant_id'])
            })

        if not applicant:
            raise Exception("No applicant found")
        client = mongo.clients.find_one({
            "_id": applicant['client_id']
        })

        applicant['client_name'] = client['client_name']

        # Look for current Check
        check = mongo["checks"].find_one({"applicant_id": applicant["_id"]})
        if not check:
            applicant['order_id_alias'] = "Not initiated"
        else:
            applicant['order_id_alias'] = check['order_id_alias']

        return {
            "status": True,
            "applicant": applicant
        }

    except Exception as e:
        return {
            "status": False,
            "message": "Error : %s" % e
        }


def wipe_uploads_of_type_for_applicant(upload_type, applicant_id):
    uploads = mongo.uploads.find({"owner_id": applicant_id, "upload_type": upload_type})
    if not uploads:
        return None
    action_log = []
    for item in uploads:
        action_entry = dict()
        action_entry['file'] = os.path.split(item["upload_path"])[-1]
        action_entry['record'] = item['_id']
        action_entry["file_wiped"] = file_wipe(item)

        result = mongo.uploads.delete_one({'_id': item['_id']})
        if result.deleted_count == 1:
            action_entry['record_wiped'] = True
        else:
            action_entry['record_wiped'] = False
        action_log.append(action_entry)
    return action_log

def file_wipe(item):
    try:
        os.remove(item["upload_path"])
        return True
    except OSError:
        return False



def create_applicant_upload(applicant_id, upload_info, f):
    applicant = app_business.lookup_user_in_collection(applicant_id, 'applicants')
    filename = secure_filename(f.filename)
    file_destination = upload_info['upload_type']
    filename = "{}_{}_".format(file_destination, applicant['last_name']) + filename
    f.filename = filename
    LOG.info(filename)
    files_path = settings.STATIC_PATH + "applicant_docs/{user_id}/{file_destination}/".format(
        user_id=str(applicant_id),
        file_destination=file_destination
    )
    inserted_doc = app_business.make_upload_doc(owner=applicant_id,
                                 owner_source="applicants",
                                 file_obj=f,
                                 upload_path=files_path,
                                 upload_info=upload_info)

    if not inserted_doc:
        LOG.info("Upload doc was not created")
        return ("Upload doc was not created")
    upl = app_business.lookup_user_in_collection(inserted_doc, "uploads")
    return upl

def wipe_uploads_if_needed(args, applicant_id, upload_type):
    for item in args:
        if item == "replace_all_existing":
            if args.get(item) == 'true':
                return wipe_uploads_of_type_for_applicant(upload_type, applicant_id)
    return None


def make_uploaded_data(inserted_doc):
    d = dict()
    d['_id'] = str(inserted_doc['_id'])
    d['upload_url'] = '/' + inserted_doc['upload_path']
    d['file_name'] = inserted_doc.get('file_name', 'No file_name')
    return d

def create_upload_info(form_data, upload_type):
    upload_info = dict()
    upload_info['upload_type'] = upload_type
    for k, v in form_data.items():
        if k in ['upload_type', '_id']:
            continue
        if v == "true":
            v = True
        elif v == "false":
            v = False
        upload_info[k] = v
    return upload_info


def generate_signed_sa(applicant_id, upload_id):
    """
    This function finds applicant agreement and attached the digital signature and add some data
    :param applicant_id: <ObjectId()> - applicant's ID
    :param upload_id: <ObjectId()> - upload_id's ID
    :return: list of document signed and uploaded on s3 bucket
    :raise: ApplicantMissingField: when digital_signature param not found
    """

    applicant = app_business.lookup_user_in_collection(applicant_id, 'applicants')
    upl = mongo.uploads.find_one({'_id': ObjectId(upload_id)})
    if 'digital_signature' not in applicant:
        raise ApplicantMissingField("digital_signature")

    upload_path = settings.STATIC_PATH + "applicant_docs/{user_id}/service_agreement/".format(user_id=applicant_id)
    upload_info = {}
    wiped = wipe_uploads_of_type_for_applicant("service_agreement", applicant_id)
    signed_docs = []

    signed = sign_paintzen_offer_letter(upl['upload_path'], applicant)
    upload_info["upload_type"] = "service_agreement"
    upload_path = upload_path + "Paintzen_Offer_Letter.pdf"
    app_business._save_new_uploaded_file(signed, upload_path)

    upload_info['file_name'] = "Paintzen_Offer_Letter.pdf"
    upload_info['file_name_short'] = "Service Agreement"
    inserted_id = app_business._register_file_upload(owner=applicant_id, owner_source='applicants', upload_path= upload_path, upload_info=upload_info)
    upl = app_business.lookup_user_in_collection(inserted_id, "uploads")
    signed_docs.append(upl)

    return signed_docs


def applicant_add_claim(applicant_id, claim_type, request_data):
    """
        This method add claim_type in applicant object for the given checks cases
        :param: applicant_id: <ObjectId()> - applicant's ID
        :param: claim_type: <str()> - claim type to be added
        :param: request_data: <dict()> - requested data
        :return: status: boolean True/False
    """
    check_code = request_data.get('check_code', None)
    case_id = request_data.get('case_id', None)
    if check_code is None or case_id is None:
        raise APIRequestFieldMissing("API request parameter for creating {} are missing".format(claim_type))
    applicant = app_business.lookup_user_in_collection(applicant_id, 'applicants')
    if not applicant:
        raise ApplicantLookupError("Applicant record not found")
    upd = dict()
    unset = dict()
    existing_claims = applicant.get('claims', dict())
    app_created_at = existing_claims.get('created_at')
    if not app_created_at:
        upd['claims.created_at'] = datetime.now()
    request_data["created_at"] = datetime.now()
    upd["claims.{}.{}".format(claim_type, case_id)] = request_data
    upd["flow_status"] = "dispute_under_review"
    unset['research_results.review_status'] = 0
    unset['research_results.review_completed_at'] = 0
    res = mongo.applicants.update({"_id": applicant_id}, {"$set": upd, '$unset': unset})
    if claim_type == "dispute":
        human_name = check_code_to_human_name.get(check_code, check_code)
        alert_text = "{} - {}".format(human_name, case_id)
        notifications_business._set_up_applicant_dispute_added_immediate_notification(applicant_id, alert_text)
        notifications_business._set_up_support_dispute_added_immediate_notification(applicant_id, alert_text)
    if res['nModified'] == 1:
        return True
    return False


def remove_applicant_middle_name(applicant_id):
    """
    Args: applicant_id: <ObjectId()> - applicant's ID
    Returns: boolean True/False
    """
    applicant_update = mongo.applicants.update({"_id": ObjectId(applicant_id)}, {'$set': {"middle_name": ""}})
    if applicant_update['nModified'] == 1:
        return True
    return False
