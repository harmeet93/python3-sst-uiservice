import flask
from app import smartscreen_api as api
from flask import request, make_response, jsonify, Response
import json
from app import mongo, LOG
from app.jwt_auth import get_jwt_identity, jwt_required, issue_access_token_for_api_user_id, jwt_refresh_token_required, issue_access_token_for_api_user, requires_permissions, get_current_user, access_authorized, get_raw_jwt, blacklist_access_token, get_routes_ui_aliases_for_api_user, create_refresh_token_for_api_user
from bson import json_util, ObjectId
import app.applicant.business as applicant_business
from app.jwt_auth import jwt_required, get_current_user, access_authorized
from app.exceptions import ApplicantUploadNotFound, ApplicantAPIv3Exception
from app.main import JSONOIDEncoder
from app.main import make_response_custom_json_encoder, compare_time_passed
from datetime import datetime
from functools import wraps
from app.utils import app_business
import os
from app.exceptions import ApplicantMissingField, ApplicantAPIv3Exception


@api.route("/v2/applicant/list_own_uploads", methods=["GET"])
@jwt_required
def list_uploads_for_applicant():
    """
    url argument: upload_type: <str> optional - recognizable upload_type.
    default: "all" - specific upload_type or "all"
    :return: list of upload documents with upload_path converted to downloadable static link
    """
    try:
        upload_type = request.args.get('upload_type', 'all')
        applicant_id = get_current_user().get_applicant_id()
        uploads = applicant_business.get_uploads_object(applicant_id, upload_type=upload_type)
        if uploads:
            response = {"status": True,
                "message": "Uploads of type '{}' found.".format(upload_type),
                "uploads": uploads}
            LOG.info("Applicant_api v3 list_uploads success for: {} with type '{}'".format(applicant_id, upload_type))
    except ApplicantUploadNotFound as e:
        response = {"status":  False,
                    "message": str(e)}
        LOG.error("Applicant_api v3 list_uploads failed with error \n {}, request.data: \n{} ".format(str(e),
                                                                                                      request.data))
    except Exception as e:
        response = {"status":  False,
                    "message": str(e)}
        LOG.exception("Applicant_api v3 list_uploads failed with error \n {}, request.data: \n{} ".format(str(e),
                                                                                                       request.data))
    return make_response(response)


@api.route("/v2/applicant/delete_own_upload/<upload_id>", methods=["DELETE"])
@jwt_required
def applicant_delete_own_upload(upload_id):
    """
    This API marks the given upload id as deleted.
    :param upload_id: Upload ObjectId  should be passed in str format.
    :return: Boolean 'status' as True/False with 'message' as str
    """
    try:
        applicant_id = get_current_user().get_applicant_id()
        res = applicant_business.delete_own_upload_applicant(applicant_id, upload_id)
        if res:
            response = {"status": True,
                        "message": 'Upload Deleted Successfully'}
        else:
            response = {"status": False,
                        "message": 'Failed To Delete Upload'}
    except ApplicantUploadNotFound as e:
        response = {"status":  False, "message": "Applicant APIv3 Error: " + str(e)}
    except Exception as e:
        response = {"status": False, "message": str(e)}
    return make_response(response)


@api.route("/v3/applicant/login", methods=["POST"])
def applicant_login():
    try:
        response = applicant_business.applicant_login(request)
        if isinstance(response, (str)):
            response = make_response(jsonify({"status": True, "message": response, }), 200)
        else:
            response = make_response(jsonify({"status": False, "message": "Combination of email address and password does not exist", }), 401)
    except Exception as e:
        response = make_response(jsonify({"status": False, "message": e, }), 401)
        LOG.exception("Applicant login failed with error \n {}, request.data: \n{} ".format(e, "request.data"))
    return response



@api.route("/applicant/info", methods=["GET"])
@jwt_required
def get_applicant_data(**kwargs):
    """
        Gets the consent info of the applicant
    """

    api_user = get_current_user()
    applicant_id = str(api_user['applicants'])
    response = applicant_business.get_applicant_data({
            "applicant_id": applicant_id
        })
    return Response(json.dumps(response, default=json_util.default), mimetype="application/json")


@api.route('/v3/get_access_token')
@jwt_refresh_token_required
def get_access_token():
    identity = get_jwt_identity()
    token = issue_access_token_for_api_user_id(identity)
    return jsonify(token)



@api.route("/v3/applicant/uploads", methods=["POST"])
@api.route("/v3/applicant/uploads/<upload_type>", methods=["POST"])
@jwt_required
def applicant_upload_file(upload_type=None):
    wiped = None
    args = request.args
    form_data = request.form
    try:
        applicant_id = get_current_user().get_applicant_id()
        f = request.files['file']
        if not f:
            raise ValueError("No file added for upload.")
        if upload_type is None:
            upload_type = request.form.get('upload_type')
        if upload_type is None:
            raise ApplicantMissingField("'upload_type' field missing")

        wiped = applicant_business.wipe_uploads_if_needed(form_data, applicant_id, upload_type)
        upload_info = applicant_business.create_upload_info(form_data, upload_type)

        inserted_doc = applicant_business.create_applicant_upload(applicant_id=applicant_id, upload_info=upload_info, f=f)
        uploaded_data = applicant_business.make_uploaded_data(inserted_doc)
        response = {"status":  True, "message": "Upload successful", "upload_doc": uploaded_data, "wiped": wiped}
    except Exception as e:
        response = {"status":  False, "message": e, "wiped": wiped}
        LOG.exception("Applicant_api v3 applicant_upload_file failed with error \n {} ".format(e))

    return Response(response=json.dumps(response, default=json_util.default), content_type="application/json")


@api.route("/v3/applicant/sign_service_agreement/<upload_id>", methods=["POST"])
@jwt_required
def applicant_signed_sa(upload_id):
    """
    This API sign the service agreements.
    :param upload_id: Upload ObjectId  should be passed in str format.
    :return: detail of the signed agreement
    """

    try:
        applicant_id = get_current_user().get_applicant_id()
        signed_docs = applicant_business.generate_signed_sa(applicant_id,upload_id)
        for sd in signed_docs:
            sd['upload_path'] = '/static' + sd['upload_path'].split("static")[1]
        response = {"status": True, "documents": signed_docs}
    except ApplicantAPIv3Exception as e:
        response = {"status":  False, "message": "Applicant APIv3 Error: " + str(e)}
    except Exception as e:
        response = {"status":  False, "message": str(e)}
        LOG.exception("Applicant_api v3 give_consent_and_sign failed with error \n {} ".format(e))

    return Response(response=json.dumps(response, default=json_util.default), content_type="application/json")


@api.route("/v3/applicant/create_dispute", methods=["POST"])
@jwt_required
def applicant_dispute_check():
    """
        This API add dispute in applicant object for the given checks cases
        :param: check_code: Check type for which dispute to be created
        :param: case_id: case number for which dispute need to be created
        :return: status: boolean True/False message: Message to display if failed
    """
    try:
        applicant_id = get_current_user().get_applicant_id()
        json_data = request.get_json(force=True)
        resp = applicant_business.applicant_add_claim(applicant_id, 'dispute', json_data)
        if resp:
            response = {"status": True, "message": "Dispute Claim Added Successfully"}
        else:
            response = {"status": False, "message": "Dispute Claim Failed"}
    except applicant_business.APIRequestFieldMissing as e:
        response = {"status":  False,
                    "message": "Applicant APIv3 Error: " + str(e)}
    except applicant_business.ApplicantLookupError as e:
        response = {"status":  False,
                    "message": "Applicant APIv3 Error: " + str(e)}
    except Exception as e:
        response = {"status":  False,
                    "message": str(e)}
        LOG.exception("Applicant_api v3 applicant_dispute_check failed with error \n {}, request.data: \n{} ".format(str(e), request.data))
    resp = Response(response=json.dumps(response, cls=JSONOIDEncoder), content_type="application/json")
    return resp


@api.route("/v3/applicant/add_comment", methods=["POST"])
@jwt_required
def applicant_comment_check():
    """
        This API add comment in applicant object for the given checks cases
        :param: check_code: Check type for which dispute to be created
        :param: case_id: case number for which dispute need to be created
        :return: status: boolean True/False message: Message to display if failed
    """
    try:
        applicant_id = get_current_user().get_applicant_id()
        json_data = request.get_json(force=True)
        resp = applicant_business.applicant_add_claim(applicant_id, 'comment', json_data)
        if resp:
            response = {"status": True, "message": "Comment Claim Added Successfully"}
        else:
            response = {"status": False, "message": "Comment Claim Failed"}
    except applicant_business.APIRequestFieldMissing as e:
        response = {"status":  False,
                    "message": "Applicant APIv3 Error: " + str(e)}
    except applicant_business.ApplicantLookupError as e:
        response = {"status":  False,
                    "message": "Applicant APIv3 Error: " + str(e)}
    except Exception as e:
        response = {"status":  False,
                    "message": str(e)}
        LOG.exception("Applicant_api v3 applicant_comment_check failed with error \n {}, request.data: \n{} ".format(str(e), request.data))
    resp = Response(response=json.dumps(response, cls=JSONOIDEncoder), content_type="application/json")
    return resp


@api.route("/v3/applicant/remove_own_middle_name", methods=["DELETE"])
@jwt_required
def remove_applicant_middle_name():
    try:
        applicant_id = get_current_user().get_applicant_id()
        resp = applicant_business.remove_applicant_middle_name(applicant_id)
        if resp:
            response = {"status": True, "message": "Middle Name Removed Successfully"}
        else:
            response = {"status": False, "message": "Failed To Remove Middle Name"}
    except Exception as e:
        response = {"status":  False,
                    "message": str(e)}
        LOG.exception("Applicant_api v3 remove_applicant_middle_name failed with error \n {}, request.data: \n{} ".format(str(e), request.data))
    resp = Response(response=json.dumps(response, cls=JSONOIDEncoder), content_type="application/json")
    return resp