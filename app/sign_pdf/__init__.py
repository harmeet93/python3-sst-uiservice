import os
import shutil
import tempfile
import PyPDF2
from PIL import Image
from app import LOG
from datetime import date, datetime
import io
import reportlab
from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib.pagesizes import letter
from reportlab.lib.utils import ImageReader
from reportlab.lib.colors import HexColor


reportlab.rl_config.TTFSearchPath.append(os.path.dirname(os.path.abspath(__file__)) + '/fonts')
pdfmetrics.registerFont(TTFont('OpenSansCondenced', 'OpenSans-CondLight.ttf'))


def put_png_string_onto_pdf(png_string, pdf_filename, signer=None, coordinates=None):
    ready_sig = prepare_signature(png_string)
    out = sign_pdf(pdf=pdf_filename, signature=ready_sig, coords=coordinates, signer=signer)
    return out


def _get_tmp_filename(suffix=".pdf"):
    with tempfile.NamedTemporaryFile(suffix=suffix) as fh:
        return fh.name


def _get_tmp_file(suffix=".pdf"):
    with tempfile.NamedTemporaryFile(suffix=suffix) as fh:
        return fh


def autocrop_image(image, border=0):
    # http://odyniec.net/blog/2012/08/auto-cropping-transparent-images-in-python-imaging-library/
    # Get the bounding box

    bbox = image.getbbox()

    # Crop the image to the contents of the bounding box
    image = image.crop(bbox)

    # Determine the width and height of the cropped image
    (width, height) = image.size

    # Add border
    width += border * 2
    height += border * 2

    # Create a new image object for the output image
    cropped_image = Image.new("RGBA", (width, height), (0, 0, 0, 0))

    # Paste the cropped image onto the new image
    cropped_image.paste(image, (border, border))

    # Done!
    return cropped_image


def prepare_signature(digital_signature):
    import base64
    # Dump transparent digital_signature to temp file
    # https://github.com/zeantsoi/remove-transparency/blob/master/remove-transparency.py
    transparent_fh = tempfile.NamedTemporaryFile(suffix=".png", mode='wb')
    png_bytes = digital_signature.split(',')[1]

    imgdata = base64.b64decode(png_bytes)
    with open(transparent_fh.name, 'wb') as f:
        img = f.write(imgdata)

    transparent_fh.write(imgdata)
    transparent_fh.flush()

    # Open temp file with transparent signature
    with open(transparent_fh.name, 'rb') as f:
      transparent_fh_read = f.read()    

    transparent_img = Image.open(transparent_fh.name)
    transparent_img = autocrop_image(image=transparent_img, border=1)
    # Create blank white image for background
    bg = Image.new('RGB', transparent_img.size, (255, 255, 255))
    # Paste transparent signature on top of white background
    bg.paste(transparent_img, (0, 0), transparent_img)
    ready_signature = tempfile.NamedTemporaryFile(suffix='.png')
    bg.save(ready_signature, "PNG")
    return ready_signature


def sign_paintzen_offer_letter(file_path, applicant):
    ready_sig = prepare_signature(applicant['digital_signature'])
    output_fname = handle_paintzen_offer_letter(file_path, applicant, ready_sig)
    return output_fname


def handle_paintzen_offer_letter(file_path, applicant, signature):
    try:
        os.remove("result.pdf")
    except Exception as e:
        pass
    try:
        os.remove("Paintzen_Offer_Letter.pdf")
    except Exception as e:
        pass

    name = "name"
    add1 = "add1"
    add2 = "add2"
    email = "email"
    phone = "phone"
    contact = "contact"
    try:
        name = applicant['signer_name']
    except KeyError:
        name = "{} {}".format(applicant['first_name'], applicant['last_name'])

    add1 = "{}".format(applicant['address']['street'])
    add2 = "{}, {}, {}".format(applicant['address']['city'], applicant['address']['state'], applicant['address']['zipcode'])
    email = applicant['email']
    phone = applicant["phone"]
    contact = "{} {}".format(applicant['first_name'], applicant['last_name'])
    sa_tempfile = tempfile.NamedTemporaryFile(suffix=".pdf", mode="wb")
    dirname = os.path.dirname(__file__)

    file_name = sa_tempfile.name.split("/")
    file_name = file_name[-1]



    def pdf(file_path, sa_tempfile, dirname, file_name):
        current_date = datetime.now()
        sign = ImageReader(signature)
        packet = io.BytesIO()
        can = canvas.Canvas(packet, pagesize=letter)
        can.drawString(352, 215, name)
        can.drawString(316, 322, add1)
        can.drawString(316, 310, add2)
        can.drawString(352, 298, email)
        can.drawString(340, 170, current_date.strftime('%m/%d/%Y'))
        can.drawImage(sign, 338, 235, 80, 15)
        can.save()

        packet.seek(0)
        new_pdf = PyPDF2.PdfFileReader(packet)
        existing_pdf = PyPDF2.PdfFileReader(open(file_path, "rb"))
        num_pages = existing_pdf.numPages
        output = PyPDF2.PdfFileWriter()

        page = existing_pdf.getPage(num_pages - 1)
        page.mergePage(new_pdf.getPage(0))
        x = existing_pdf.getNumPages()

        for n in range(x):
            output.addPage(existing_pdf.getPage(n))

        with open( sa_tempfile.name , "wb") as out_file:
            output.write(out_file)

        return sa_tempfile, output

    sa_tempfile, output = pdf(file_path, sa_tempfile, dirname, file_name)
    def datet(sa_tempfile, dirname, file_name):
        current_date = datetime.now()
        packet = io.BytesIO()
        can = canvas.Canvas(packet, pagesize=letter)
        can.drawString(455, 671, current_date.strftime('%m/%d/%Y'))
        can.drawString(260, 617, name)
        can.drawString(260, 602, add1)
        can.drawString(260, 590, add2)
        can.drawString(260, 548, email)
        can.drawString(260, 560, phone)
        can.drawString(260, 573, contact)
        can.save()

        packet.seek(0)
        new_pdf = PyPDF2.PdfFileReader(packet)
        sa_tempfile.seek(0)
        file_new = dirname +"/" + file_name
        existing_pdf = PyPDF2.PdfFileReader(open(sa_tempfile.name, "rb"))
        num_pages = existing_pdf.numPages
        output = PyPDF2.PdfFileWriter()

        page = existing_pdf.getPage(num_pages - 1)
        page.mergePage(new_pdf.getPage(0))
        x = existing_pdf.getNumPages()

        for n in range(x):
            output.addPage(existing_pdf.getPage(n))

        with open(sa_tempfile.name, "wb") as out_file:
            output.write(out_file)

        sa_tempfile.flush()
        sa_tempfile.seek(0)
        return sa_tempfile
    return open(sa_tempfile.name, "rb")


def sign_pdf(pdf, signature, coords=None, signer=None):
    # https://github.com/yourcelf/signpdf/blob/master/signpdf.py
    if coords is not None:
        page_num, x1, y1, width, height = [int(a) for a in coords.split("x")]
        page_num -= 1
    else:
        page_num = 0


    signature = signature.name

    # output_filename = "{}_signed{}".format(
    #     *os.path.splitext(pdf)
    # )

    pdf_fh = open(pdf, 'rb')
    sig_tmp_fh = None

    pdf = PyPDF2.PdfFileReader(pdf_fh)
    writer = PyPDF2.PdfFileWriter()
    sig_tmp_filename = None
    page_num = pdf.getNumPages() - 1
    for pp in range(page_num):
        writer.addPage(pdf.getPage(pp))

    page = pdf.getPage(page_num)

    # Get page dimensions to put signature to the bottom of page
    p_size = page.cropBox
    # LOG.info(p_size)
    if coords is None:
        page_width = p_size[2]
        page_height = p_size[3]
        width = page_width // 8
        height = page_height // 8
        x1 = page_width / 8 * 3
        y1 = page_height / 8 * 6.8

    line_approx = page_height//60
    LOG.debug("page height %d" % page_height)
    LOG.debug("line height is approx %d" % line_approx)

    # Create PDF for signature
    sig_tmp_filename = _get_tmp_filename()
    c = canvas.Canvas(sig_tmp_filename, pagesize=(p_size[2], p_size[3]))
    drawn = c.drawImage(signature, x1, 0 + line_approx*3, width, height,
                        mask=[250, 255, 250, 255, 250, 255],
                        preserveAspectRatio=True, anchor='s')

    c.setFont("Helvetica", size=10)
    # c.drawRightString(x1, y1+height, "Signed by: {} \n Signed on")
    if signer is not None:
        signer_name = signer.get("signer_name")
        if not signer_name:
            signer_name = "{} {}".format(signer['first_name'], signer['last_name'])

        c.drawString(x1, 0 + line_approx*3, "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _")
        c.drawString(x1, 0 + line_approx*2, "Signed by: " + signer_name)
        c.drawString(x1, 0 + line_approx, "Signed on: {}".format(date.today().strftime("%B %d, %Y")))


    c.showPage()
    c.save()

    # Merge PDF in to original page
    sig_tmp_fh = open(sig_tmp_filename, 'rb')
    sig_tmp_pdf = PyPDF2.PdfFileReader(sig_tmp_fh)
    sig_page = sig_tmp_pdf.getPage(0)
    sig_page.mediaBox = page.mediaBox
    page.mergePage(sig_page)

    writer.addPage(page)

    fh = tempfile.NamedTemporaryFile(suffix=".pdf", mode="r+")
    writer.write(fh)
    fh.flush()
    fh.seek(0)

    for handle in [pdf_fh, sig_tmp_fh]:
        if handle:
            handle.close()
    if sig_tmp_filename:
        os.remove(sig_tmp_filename)

    return fh


def sign_w9_form(file_path, applicant):
    ready_sig = prepare_signature(applicant['digital_signature'])
    output_fname = handle_w9_from(file_path, applicant, ready_sig)
    return output_fname


def handle_w9_from(file_path, data, ready_sig):
    # data = {
    #     "ssn": "111-11-1111",
    #     "exempt_code": "C12345",
    #     "fatca_code": "F12345",
    #     "full_name": "Jenny Roy",
    #     "user_state": "Florida",
    #     "full_address": "G-9, Alexa Aprtment, M.G. Road",
    #     "user_city": "Alva",
    #     "city_zipcode": "12345",
    #     "business_name": "TCL",
    #     "employee_identynumber": "22-2222222",
    #     "w9form_option": "4"
    # }
    data = data["w9form_data"]
    # res = ""
    # for char in data["social_securitynumber"]:
    #     res += char + "  "
    # data["social_securitynumber"] = res

    current_date = datetime.now()
    c = canvas.Canvas(file_path)
    # form = c.acroForm
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)
    sign = ImageReader(ready_sig)

    can.setFillColor(HexColor(0x1F6EFE))
    can.drawString(68, 688, data.get('full_name'))
    coords_to_options = {
        "1": {"x": 67, "y": 638, "text": u'\u2713'},
        "2": {"x": 188, "y": 638, "text": u'\u2713'},
        "3": {"x": 261, "y": 638, "text": u'\u2713'},
        "4": {"x": 326, "y": 638, "text": u'\u2713'},
        "5": {"x": 398, "y": 638, "text": u'\u2713'},
        "6": {"x": 67, "y": 620, "text": u'\u2713'},
    }
    can.drawString(**coords_to_options.get(data.get("w9form_option")))

    # form.checkbox(x=110, y=645, buttonStyle='check')
    can.drawString(68, 662, data.get('business_name'))
    can.drawString(540, 625, data.get('exempt_code'))
    can.drawString(505, 602, data.get('fatca_code'))

    addr = data.get('user_city') + ', ' + data.get('user_state') + ', ' + data.get('city_zipcode')
    can.drawString(68, 542, addr)

    can.drawString(423, 476, data.get('social_securitynumber'), charSpace=8)
    can.drawString(421.5, 428.25, data.get('employee_identynumber'), charSpace=8)
    can.drawString(68, 568, data.get('full_address'))
    if "csp_code" in data:
        can.drawString(420, 620, data.get('csp_code'))

    # can.drawString(323, 638, u'\u2713')
    can.drawImage(sign, 130, 257, 80, 15)
    can.drawString(410, 255, current_date.strftime('%m/%d/%Y'))

    can.save()

    packet.seek(0)
    new_pdf = PyPDF2.PdfFileReader(packet)

    existing_pdf = PyPDF2.PdfFileReader(file(file_path, "rb"))
    output = PyPDF2.PdfFileWriter()

    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)

    outputStream = tempfile.NamedTemporaryFile(suffix=".pdf", mode="r+")
    output.write(outputStream)
    outputStream.flush()
    outputStream.seek(0)
    return outputStream



def sign_mvr_driver_form(file_path, applicant, dl_short_state):
    ready_sig = None
    if 'digital_signature' in applicant:
        ready_sig = prepare_signature(applicant['digital_signature'])

    if dl_short_state == 'WA':
        output_fname = handle_wa_driver_from(file_path, applicant, ready_sig)

    if dl_short_state == 'PA' : 
        output_fname = handle_pa_driver_from(file_path, applicant, ready_sig)

    if dl_short_state == 'NH' : 
        output_fname = handle_nh_driver_from(file_path, applicant, ready_sig)

    return output_fname


def handle_pa_driver_from(file_path, data, ready_sig):
    LOG.debug(data)
    data = data["mvr_driver_data"]
    current_date = datetime.now()
    c = canvas.Canvas(file_path)
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)
    can.setFillColor(HexColor(0x1F6EFE))

    if ready_sig is not None:
        sign = ImageReader(ready_sig)
        can.drawImage(sign, 60, 195, 80, 10)

    can.drawString(310, 585, data.get('client_name'))
    can.drawString(450, 515, data.get('client_phone'))
    can.drawString(430, 500, data.get('relation'))
    can.drawString(310, 526, data.get('client_city'))
    can.drawString(520, 526, data.get('client_state'))
    can.drawString(545, 526, data.get('client_zip'))
    can.drawString(310, 552, data.get('client_address'))
    can.drawString(180, 407, data.get('first_name'))
    can.drawString(70, 407, data.get('last_name'))
    can.drawString(70, 280, data.get('dob')[2])
    can.drawString(45, 280, data.get('dob')[1])
    can.drawString(95, 280, data.get('dob')[0])    
    can.drawString(100, 245, data.get('full_name'))
    can.drawString(70, 364, data.get('city'))
    can.drawString(70, 340, data.get('dl_state'))
    can.drawString(255, 340, data.get('zipcode'))
    can.drawString(150, 285, data.get('dl_number'))
    can.drawString(70, 385, data.get('full_address'))
    can.drawString(70, 316, data.get('phone_number'))
    can.drawString(250, 195, current_date.strftime('%m/%d/%Y'))

    can.save()

    packet.seek(0)
    new_pdf = PyPDF2.PdfFileReader(packet)

    existing_pdf = PyPDF2.PdfFileReader(file(file_path, "rb"))
    output = PyPDF2.PdfFileWriter()

    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)

    outputStream = tempfile.NamedTemporaryFile(suffix=".pdf", mode="r+")
    output.write(outputStream)
    outputStream.flush()
    outputStream.seek(0)
    return outputStream


def sign_ca_disclosure(file_path, data):
    ready_sig = None
    if 'digital_signature' in data['applicant']:
        ready_sig = prepare_signature(data['applicant']['digital_signature'])

    LOG.debug(data)
    current_date = datetime.now()
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)

    # Put signature
    if ready_sig is not None:
        sign = ImageReader(ready_sig)
        can.drawImage(sign, 100, 190, 100, 20)

    # Put white rectangle for "Employer" placeholder
    can.setFillColorRGB(255, 255, 255)
    can.rect(33, 702, 60, 15, stroke=0, fill=1)

    # Put employer name
    can.setFillColorRGB(0, 0, 0)
    can.setFont('OpenSansCondenced', 10)
    can.drawString(35, 707, data.get('client_name'))

    # Put applicant name and date
    can.setFont('OpenSansCondenced', 11)
    if "signer_name" in data['applicant']:
        can.drawString(100, 213, "{}".format(data['applicant']['signer_name']))
    else:
        can.drawString(100, 213, "{} {}".format(data['applicant'].get('first_name', ''), data['applicant'].get('last_name', '')))
    can.drawString(330, 195, current_date.strftime('%m/%d/%Y'))

    can.save()

    packet.seek(0)
    new_pdf = PyPDF2.PdfFileReader(packet)

    existing_pdf = PyPDF2.PdfFileReader(file(file_path, "rb"))
    output = PyPDF2.PdfFileWriter()

    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)

    outputStream = tempfile.NamedTemporaryFile(suffix=".pdf", mode="r+")
    output.write(outputStream)
    outputStream.flush()
    outputStream.seek(0)
    return outputStream


def sign_bc_authorization(file_path, data):
    ready_sig = None
    if 'digital_signature' in data['applicant']:
        ready_sig = prepare_signature(data['applicant']['digital_signature'])

    LOG.debug(data)
    current_date = datetime.now()
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)

    # Put signature
    if ready_sig is not None:
        sign = ImageReader(ready_sig)
        can.drawImage(sign, 130, 310, 100, 20)

    # Put white rectangle for "Employer" placeholder
    can.setFillColorRGB(255, 255, 255)
    can.rect(152, 656, 60, 15, stroke=0, fill=1)

    # Put employer name
    can.setFont('OpenSansCondenced', 10)
    can.setFillColorRGB(0, 0, 0)
    can.drawString(155, 659, data.get('client_name'))

    # Put applicant name and date
    can.setFont('OpenSansCondenced', 11)
    if "signer_name" in data['applicant']:
        can.drawString(230, 310, "{}".format(data['applicant']['signer_name']))
    else:
        can.drawString(230, 310, "{} {}".format(
            data['applicant'].get('first_name', ''), data['applicant'].get('last_name', '')))
    can.drawString(430, 310, current_date.strftime('%m/%d/%Y'))

    can.save()

    packet.seek(0)
    new_pdf = PyPDF2.PdfFileReader(packet)

    existing_pdf = PyPDF2.PdfFileReader(file(file_path, "rb"))
    output = PyPDF2.PdfFileWriter()

    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)

    outputStream = tempfile.NamedTemporaryFile(suffix=".pdf", mode="r+")
    output.write(outputStream)
    outputStream.flush()
    outputStream.seek(0)
    return outputStream
    pass


def sign_bc_disclosure(file_path, data):
    ready_sig = None
    if 'digital_signature' in data['applicant']:
        ready_sig = prepare_signature(data['applicant']['digital_signature'])

    LOG.debug(data)
    current_date = datetime.now()
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)

    # Put signature
    if ready_sig is not None:
        sign = ImageReader(ready_sig)
        can.drawImage(sign, 150, 522, 100, 20)

    # Put white rectangle for "Employer" placeholder
    can.setFillColorRGB(255, 255, 255)
    can.rect(60, 721, 70, 15, stroke=0, fill=1)

    # Put employer name
    can.setFont('OpenSansCondenced', 10)
    can.setFillColorRGB(0, 0, 0)
    can.drawString(65, 726, data.get('client_name'))

    can.setFont('OpenSansCondenced', 12)

    # Put applicant name and date
    if "signer_name" in data['applicant']:
        can.drawString(150, 545, "{}".format(data['applicant']['signer_name']))
    else:
        can.drawString(150, 545, "{} {}".format(
            data['applicant'].get('first_name', ''), data['applicant'].get('last_name', '')))
    can.drawString(430, 522, current_date.strftime('%m/%d/%Y'))

    can.save()

    packet.seek(0)
    new_pdf = PyPDF2.PdfFileReader(packet)

    existing_pdf = PyPDF2.PdfFileReader(file(file_path, "rb"))
    output = PyPDF2.PdfFileWriter()

    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)

    outputStream = tempfile.NamedTemporaryFile(suffix=".pdf", mode="r+")
    output.write(outputStream)
    outputStream.flush()
    outputStream.seek(0)
    return outputStream
    pass


def handle_wa_driver_from(file_path, data, ready_sig):
    data = data["mvr_driver_data"]
    current_date = datetime.now()
    c = canvas.Canvas(file_path)
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)
    can.setFillColor(HexColor(0x1F6EFE))

    if ready_sig is not None:
        sign = ImageReader(ready_sig)
        can.drawImage(sign, 100, 422, 80, 10)

    can.drawString(350, 708, data.get('client_name'))
    can.drawString(430, 690, data.get('VettyInc'))
    can.drawString(100, 576, data.get('full_name'))
    can.drawString(100, 445, data.get('full_name'))
    can.drawString(370, 446, data.get('dl_number'))
    can.drawString(370, 422, current_date.strftime('%m/%d/%Y'))
    can.drawString(70, 181, data.get('VettyInc'))
    can.drawString(290, 181, data.get('SubratNayak'))
    can.drawString(470, 181, data.get('CEO'))
    can.drawString(70, 158, data.get('VettyAdd'))
    can.drawString(70, 122, current_date.strftime('%m/%d/%Y'))
    can.save()

    packet.seek(0)
    new_pdf = PyPDF2.PdfFileReader(packet)

    existing_pdf = PyPDF2.PdfFileReader(file(file_path, "rb"))
    output = PyPDF2.PdfFileWriter()

    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)

    outputStream = tempfile.NamedTemporaryFile(suffix=".pdf", mode="r+")
    output.write(outputStream)
    outputStream.flush()
    outputStream.seek(0)
    return outputStream


def handle_nh_driver_from(file_path, data, ready_sig):
    data = data["mvr_driver_data"]
    current_date = datetime.now()
    c = canvas.Canvas(file_path)
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)
    can.setFillColor(HexColor(0x1F6EFE))

    if ready_sig is not None:
        sign = ImageReader(ready_sig)

    can.drawString(370, 603, data.get('client_name'))
    can.drawString(300, 550, data.get('client_address'))
    can.drawString(290, 523, data.get('client_city'))
    can.drawString(470, 523, data.get('client_state'))
    can.drawString(520, 523, data.get('client_zip'))
    can.drawString(90, 128, '{}/{}/{}'.format(data.get('dob')[1], data.get('dob')[2],data.get('dob')[0])) 
    can.drawString(60, 146, data.get('full_name'))
    can.drawString(125, 91, data.get('dl_number'))
    can.drawString(43, 582, data.get('x'))
    can.drawString(25, 450, data.get('x'))
 
    can.save()

    packet.seek(0)

    packet1 = io.BytesIO()
    can = canvas.Canvas(packet1, pagesize=letter)
    can.setFillColor(HexColor(0x1F6EFE))

    if ready_sig is not None:
        sign = ImageReader(ready_sig)
        can.drawImage(sign, 90, 655, 80, 10)
        can.drawImage(sign, 370, 585, 80, 10)

    can.drawString(100, 606, data.get('full_name'))
    can.drawString(280, 650, current_date.strftime('%m/%d/%Y'))

    can.save()

    packet1.seek(0)

    new_pdf = PyPDF2.PdfFileReader(packet)
    new_pdf1 = PyPDF2.PdfFileReader(packet1)

    existing_pdf = PyPDF2.PdfFileReader(file(file_path, "rb"))
    num_pages = existing_pdf.numPages
    output = PyPDF2.PdfFileWriter()
    page = existing_pdf.getPage(num_pages-2)
    page1 = existing_pdf.getPage(num_pages-1)
    page.mergePage(new_pdf.getPage(0))
    page1.mergePage(new_pdf1.getPage(0))

    x = existing_pdf.getNumPages()
    for n in range(x):
        output.addPage(existing_pdf.getPage(n))
    outputStream = tempfile.NamedTemporaryFile(suffix=".pdf", mode="r+")
    output.write(outputStream)
    outputStream.flush()
    outputStream.seek(0)
    return outputStream

