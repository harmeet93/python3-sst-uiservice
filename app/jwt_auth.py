from flask import jsonify, request, make_response
from werkzeug.security import check_password_hash
from functools import wraps
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, decode_token, create_refresh_token, jwt_optional, get_jwt_identity
from flask_jwt_extended import get_jwt_claims, jwt_refresh_token_required, get_current_user, get_raw_jwt
import datetime
from simplekv.db.mongo import MongoStore
from bson import json_util, ObjectId
from app import mongo
from app import smartscreen_api as api, LOG
from collections import MutableMapping
# api.config['JWT_BLACKLIST_STORE'] = MongoStore(mongo, 'refresh_tokens') #deprecation_refresh_tokens
api.config['JWT_BLACKLIST_ENABLED'] = True
api.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

jwt = JWTManager(api)


class InvalidDaysValueForExpire(Exception):
    pass

class NoRecordRegistered(Exception):
    pass


class APIMAsterUser(MutableMapping):

    def __init__(self, api_user):
        self.__dict__.update(api_user)

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def __getitem__(self, key):
        return self.__dict__[key]

    def __delitem__(self, key):
        del self.__dict__[key]

    def __iter__(self):
        return iter(self.__dict__)

    def __len__(self):
        return len(self.__dict__)

    def get_admin_user_record(self):
        admin_user_id = self.get_admin_user_id()
        LOG.debug(admin_user_id)
        admin_user = mongo.admin_user.find_one({"_id": admin_user_id})
        return admin_user

    def get_client_user_record(self):
        client_user_id = self.get_client_user_id()
        client_user = mongo.client_user.find_one({"_id": client_user_id})
        return client_user

    def get_admin_user_id(self):
        admin_user_id = self.get('admin_user', None)
        if admin_user_id is None:
            raise NoRecordRegistered("admin_user_id profile for current admin is not found.")
        return admin_user_id

    def get_applicant_record(self):
        applicant_id = self.get_applicant_id()
        applicant = mongo.applicants.find_one({"_id": applicant_id})
        if not applicant:
            raise ValueError("Applicant {} lookup failed.".format(applicant_id))
        else:
            return applicant

    def get_client_user_id(self):
        client_user_id = self.get('client_user', None)
        if client_user_id is None:
            raise NoRecordRegistered("client_user profile for current user is not found.")
        return client_user_id

    def get_applicant_id(self):
        applicant_id = self.get('applicants', None)
        if applicant_id is None:
            raise NoRecordRegistered("Applicant profile for current user is not found.")
        return applicant_id

    def get_researcher_id(self):
        researcher_id = self.get('researcher', None)
        if researcher_id is None:
            raise NoRecordRegistered("Researcher profile for current user is not found.")
        return researcher_id

    def get_researcher_record(self):
        researcher_id = self.get_researcher_id()
        researcher = mongo.researcher.find_one({"_id": researcher_id})
        if not researcher:
            raise ValueError("Researcher {} lookup failed.".format(researcher_id))
        return researcher

    def get_client_users_client_id(self):
        client_user_record = self.get_client_user_record()
        try:
            return client_user_record['client_id']
        except Exception as e:
            raise ValueError("No client configured for api_user's client_user")

    def get_onboarding_user_id(self):
        client_user_id = self.get_client_user_id()
        return client_user_id

    def is_allowed_to_access_route_id(self, route):
        perms_list = self.get('permissions', [])
        # permissions = mongo['permissions'].find({'_id': {'$in': perms_list}})
        for perm in perms_list:
            try:
                if route in RESOLVED_PERMISSIONS[perm]:
                    LOG.debug(
                        "Is allowed: route id {} found under permission id {} ".format(route, perm)
                    )
                    return True
            except KeyError:
                continue

        user_groups = self.get('user_groups', [])
        for group in user_groups:
            if route in RESOLVED_GROUP_PERMISSIONS[group]:
                LOG.debug(
                    "Is allowed: route id {} found group id {} ".format(route, group)
                )
                return True

        else:
            LOG.debug(
                    "Is allowed: route id {} is FALSE".format(route)
                )
            return False

    def check_for_temp_blocked(self):
        try:
            login_attempt = self.get("login_attempt")
            if login_attempt.get("blocked_at"):
                blocked_duration = datetime.datetime.now() - login_attempt.get("blocked_at")
                blocked_mins = divmod(blocked_duration.total_seconds(), 60)[0]
                if ( blocked_mins < 15.0 ):
                    return ("Your account is temporary blocked please try after %d minutes" % int(15 - blocked_mins))
            return False        
        except Exception as e:
            print ("error", e)
            return False

    def check_for_change_pass(self):
        try:
            if (datetime.datetime.now() - self.get("password_updated_at")).days >= 120:
                return "Your account is blocked please change your password"
            return False
        except Exception as e:
            return False


    def invalid_password_attempt(self):
        try:
            login_attempt = self.get("login_attempt")
            msg = "Combination of email address and password does not exist"

            if login_attempt.get("created_at"):
                created_at = login_attempt.get("created_at")
            else:
                created_at = datetime.datetime.now()    
            if login_attempt.get("attempts"):
                attempts = login_attempt.get("attempts")
            else:
                attempts = 0    

            attempt_duration = datetime.datetime.now() - created_at
            attempt_hours = divmod(attempt_duration.total_seconds(), 3600)[0]            

            if created_at and attempt_hours < 1.0:
                attempts = attempts + 1
            else:
                created_at = datetime.datetime.now()
                attempts = 1    

            if attempts >= 5:
                attempts = 0
                blocked_at = datetime.datetime.now()
                msg = "Your account is temporary blocked please try after 15 minutes"
            else:
                blocked_at = login_attempt.get("blocked_at")        
            
            applicant = mongo.api_users.update({ 
                "email": self.get("email")
            },{
                "$set" : {
                    "login_attempt" : {
                        'attempts' : attempts,
                        'created_at' : created_at,
                        'blocked_at' : blocked_at,
                    }
                }
            })
            return msg    
        except Exception as e:
            return "Combination of email address and password does not exist"

    def update_attempt_protection(self):
        try:
            applicant = mongo.api_users.update({ 
                "email": self.get("email")
            },{
                "$set" : {
                    "login_attempt" : {
                        'attempts' : 0,
                        'created_at' : None,
                        'blocked_at' : None,
                    }
                }
            })
            return True
        except Exception as e:
            return False            

    def match_old_pwds(self, new_pass):
        try:
            if self.get("old_passwords"):
                for password in self.get("old_passwords"):
                    if check_password_hash(password, new_pass):
                        raise Exception("New password should not be match with last 5 passwords")
            else:
                for source in self['sources']:
                    source_detail = mongo[source].find_one({"_id": self[source]})
                    if check_password_hash(source_detail.get("password"), new_pass):
                        raise Exception("New password should not be match with last 5 passwords")
        except Exception as e:
            raise e
                

    def update_old_pwd(self):
        old_passwords = self.get("old_passwords")
        if len(old_passwords) == 5:
            old_passwords.remove(old_passwords[0])
        old_passwords.append(self.get("password"))
        mongo.api_users.update_one({"_id": self['_id']}, {"$set": {"old_passwords": old_passwords, "password_updated_at" : datetime.datetime.now()}})


@jwt.user_claims_loader
def add_claims_to_access_token(api_user):
    # roles = api_user.get("roles", None)
    user_groups = api_user.get('user_groups', [])
    permissions = api_user.get("permissions", [])
    permissions = [str(perm) for perm in permissions]
    user_groups = [str(group) for group in user_groups]
    return {
        'user_groups': user_groups,
        'permissions': permissions
    }


@jwt.user_identity_loader
def user_identity_lookup(api_user):
    try:
        return json_util.dumps(api_user["_id"])
    except TypeError:
        raise ValueError("Unable to find api_user wrapper for current user.")


@jwt.user_loader_callback_loader
def user_loader_callback(identity):
    oid = json_util.loads(identity)
    api_user = mongo.api_users.find_one({"_id": oid})
    if not api_user:
        raise NoRecordRegistered("No user for given token")
    api_user = APIMAsterUser(api_user)
    return api_user


def insufficient_permissions(permissions):
    response = make_response(jsonify({
        "success": False,
        "message": "For this route you need following permissions: {}. Please contact your administrator.".format(
            permissions)
    }), 403)
    return response


def requires_permissions(*permissions):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            api_user_permisisons = get_jwt_claims()['permissions']
            LOG.debug("Checking required permissions {}".format(permissions))
            LOG.debug("Against user permissions: {}".format(api_user_permisisons))
            if api_user_permisisons is None:
                return insufficient_permissions(permissions)
            for perm in permissions:
                if perm not in api_user_permisisons:
                    return insufficient_permissions(permissions)
            return f(*args, **kwargs)
        return wrapped
    return wrapper


def insufficient_roles(roles):
    response = make_response(jsonify({
        "success": False,
        "message": "For this route your account roles should be: {}. Please contact your administrator.".format(roles)
    }), 403)
    return response


def requires_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            api_user_roles = get_jwt_claims()['roles']
            if api_user_roles is None:
                return insufficient_roles(roles)
            for perm in roles:
                if perm not in api_user_roles:
                    return insufficient_roles(roles)
            return f(*args, **kwargs)
        return wrapped
    return wrapper


def access_authorized(f):
    # LOG.debug("decorator recieved args: \n {}".format(f))

    @wraps(f)
    def wrapped(*args, **kwargs):
        rule = request.url_rule.rule
        method = request.method
        route = mongo['routes'].find_one({'rule': rule, 'methods': method})
        api_user = get_current_user()
        if api_user.is_allowed_to_access_route_id(route['_id']):
            LOG.debug('Access authorized is True')
            return f(*args, **kwargs)
        else:
            LOG.debug('Access authorized is False')
            return make_response(jsonify({"status": False,
                            "message": 'You are not authorized to perform this action. If you are confident '
                                       'otherwise please contact support.'}), 403)
    return wrapped


def issue_access_token_for_api_user_id(identity):
    api_user = mongo["api_users"].find({"_id": json_util.loads(identity)})[0]
    return create_access_token(identity=api_user, expires_delta=datetime.timedelta(hours=24))


def issue_access_token_for_api_user(api_user):
    return create_access_token(identity=api_user, expires_delta=datetime.timedelta(hours=24))


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    token = mongo['blacklisted_tokens'].find_one({"jti": jti})
    if token:
        return True
    else:
        return False


def blacklist_access_token(jti, expires=None, reason="Unknown"):
    if expires is None:
        expires = datetime.datetime(1, 1, 1)

    doc = {"jti": jti,
           "type": "access",
           "expires": expires,
           "reason": reason,
           }
    LOG.debug("blacklist_access_token: \n{}".format(doc))
    result = mongo['blacklisted_tokens'].update_one({"jti": jti}, {"$set": doc}, upsert=True)
    LOG.debug("blacklist result: \n {}".format(result))


def create_refresh_token_for_api_user(api_user, days_till_expire=None):
    if days_till_expire is None:
        expires = datetime.timedelta(days=365)
    else:
        if not 0 < days_till_expire < 365:
            raise InvalidDaysValueForExpire("Value for days till expiration must be an int between 0 and 365")
        else:
            expires = datetime.timedelta(days=days_till_expire)
    token = create_refresh_token(identity=api_user, expires_delta=expires)
    expiration_date = datetime.datetime.now() + expires
    return token, expiration_date


def get_permissions(api_user_record):
    permissions = list(mongo.permissions.find({'_id': {'$in': api_user_record.get('permissions', list())}}))
    return permissions


def get_routes_ui_aliases_for_api_user(user):
    permissions = get_permissions(api_user_record=user)
    allowed_routes = list()
    for p in permissions:
        allowed_routes += p.get('routes', list())
    routes = list(mongo.routes.find({'_id': {'$in': allowed_routes}}))
    allowed_actions = list()
    for route in routes:
        allowed_actions += route.get('ui_aliases', list())
    return allowed_actions    

