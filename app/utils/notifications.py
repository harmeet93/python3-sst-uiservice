import os
from datetime import datetime, timedelta
from app import LOG


class ReccurencySettings:
    daily = {
            'period_value': 1,
            'period_units': 'days'
        }


class TimeTable:
    rec_settings = None

    def __init__(self):
        self.rec_settings = ReccurencySettings()

    @property
    def today(self):
        return datetime.now()

    @property
    def tomorrow(self):
        return datetime.now() + timedelta(days=1)

    @property
    def in_2_days(self):
        return datetime.now() + timedelta(days=2)

    @property
    def in_5_days(self):
        return datetime.now() + timedelta(days=5)

    @property
    def in_6_days(self):
        return datetime.now() + timedelta(days=6)

    @property
    def in_7_days(self):
        return datetime.now() + timedelta(days=7)

    def in_n_days(self, n):
        if not isinstance(n, int):
            raise TypeError("Value for the N - number of days should be of 'int' type")
        return datetime.now() + timedelta(days=n)


class TestRecurrencySettings:

    daily = {
            'period_value': 5,
            'period_units': 'minutes'
        }


class TestTimeTable(TimeTable):
    rec_settings = None

    def __init__(self):
        self.rec_settings = TestRecurrencySettings()

    @property
    def today(self):
        return datetime.now()

    @property
    def tomorrow(self):
        return datetime.now() + timedelta(minutes=5)

    @property
    def in_2_days(self):
        return datetime.now() + timedelta(minutes=10)

    @property
    def in_5_days(self):
        return datetime.now() + timedelta(minutes=25)

    @property
    def in_6_days(self):
        return datetime.now() + timedelta(minutes=30)

    @property
    def in_7_days(self):
        return datetime.now() + timedelta(minutes=45)

    def in_n_days(self, n):
        if not isinstance(n, int):
            raise TypeError("Value for the N - number of days should be of 'int' type")
        return datetime.now() + timedelta(minutes=n * 5)


LOG.info("using {} deployment env for notifications".format(os.getenv("DEPLOYMENT_ENV")))
if os.getenv("DEPLOYMENT_ENV") == "staging":
    T = TestTimeTable()
else:
    T = TimeTable()