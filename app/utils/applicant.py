check_code_to_human_name = {
        'crimcounty': "Criminal County",
        'ssntrace': "SSN Trace",
        'nationalcrim': "National Criminal",
        'drivinglicense': "Driving Record",
        'fedcrim': "Federal Criminal",
        'drugs_5pan': "5 Panel Drug Screen",
        'drugs_7pan': "7 Panel Drug Screen",
        'drugs_9pan': "9 Panel Drug Screen",
        'drugs_10pan': "10 Panel Drug Screen",
        'sexof': "Sex Offender",
        'gstw': "Terrorist Watch List",
        'drivrec': "Motor Vehicle Record",
        'countycivil': "County Civil",
        "oig": "OIG",
        "gsa_sam": "GSA/SAM"
    }