from re import compile, VERBOSE
import random, string, inspect
from itsdangerous import URLSafeTimedSerializer
from app import mongo, LOG, smartscreen_api as api
from json import loads
from re import compile, VERBOSE
from urllib.request import urlopen
from bson import ObjectId
import datetime
import json
from flask import Response

FREE_GEOIP_URL = "http://freegeoip.net/json/{}"
VALID_IP = compile(r"""
\b
(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
\b
""", VERBOSE)

SECRET_KEY = api.config['SECRET_KEY']
SECURITY_PASSWORD_SALT = api.config['SECURITY_PASSWORD_SALT']





def decrypt_timeless_token(token,  secret_key=SECRET_KEY, salt=SECURITY_PASSWORD_SALT):
    """confirms the token without any expiry restriction
    :param token: token to be confirmed
    :param secret_key: secret key for encryption / decryption, using app's key
    :param salt: SALT for encryption/ decryption, using apps config salt
    :returns: False if wrong token, decrypted token if valid
    """
    serializer = URLSafeTimedSerializer(secret_key)
    try:
        decrypted_token = serializer.loads(
            token,
            salt=salt
        )
    except:
        return False
    return decrypted_token

def generate_random_string(len=10):
    """Generates a random string of the specified length.

    :param len: The length of the string that should be generated. Defaults
    to 10.
    :return: str -- The random string
    """
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in
                   range(len))

def encrypt_token(token, secret_key=SECRET_KEY, salt=SECURITY_PASSWORD_SALT):
    """generates the encrypted token for emails
    :param token: token to be encrypted
    :param secret_key: secret key for encryption / decryption, using app's key
    :param salt: SALT for encryption/ decryption, using apps config salt
    """
    serializer = URLSafeTimedSerializer(secret_key)
    return serializer.dumps(str(token), salt=salt)


def decrypt_timed_token(token, secret_key=SECRET_KEY, salt=SECURITY_PASSWORD_SALT, expiration=86400):
    """decrypts the token with expiry period
    :param token: token to be confirmed
    :param secret_key: secret key for encryption / decryption, using app's key
    :param salt: SALT for encryption/ decryption, using apps config salt
    :param expiration: expiry duration of the token
    :returns: False if wrong token, decrypted token if valid
    """
    serializer = URLSafeTimedSerializer(secret_key)
    try:
        decrypted_token = serializer.loads(
            token,
            salt=salt,
            max_age=expiration
        )
    except:
        return False
    return decrypted_token

