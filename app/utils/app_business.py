import os, shutil, json, re
import xlrd
import jinja2
from datetime import datetime
import settings
from app.utils import notifications
from bson import json_util, ObjectId
from app import smartscreen_api as api
import json
from app import mongo, LOG
from app.jwt_auth import APIMAsterUser
from base64 import b64encode
from werkzeug.security import check_password_hash, generate_password_hash
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, decode_token, create_refresh_token, jwt_optional, get_jwt_identity
from flask_jwt_extended import get_jwt_claims, jwt_refresh_token_required, get_current_user, get_raw_jwt
import boto3
from datetime import datetime
from werkzeug.utils import secure_filename
from vetty_utils import vetty_send_mail, vetty_send_sms
from vetty_utils.vetty_send_mail import send_mail
from flask import Flask, render_template
import jinja2

# res =  send_mail(email_to=email_to, email_from=email_from, subject=subject, email_content=message)




class V1BusinessException(Exception):
    pass


class UploadExists(V1BusinessException):
    pass



def send_email_with_template(email_to, subject, context, template_path, email_from=None):
    """
    This function send email templates using vetty utills 
    """

    msg = render_template(template_path, response = context)

    email_response = send_mail(
        email_to=email_to,
        email_from=email_from or api.config['DEFAULT_EMAIL'],
        subject=subject,
        email_content=msg
    )
    response = {"status_code": email_response.status_code, "body":  email_response.body}
    if email_response.status_code == 202:
        response['status_code'] = email_response.status_code
        response['body'] = "Email sent successfully"
    return response


def _register_file_upload(owner, owner_source, upload_path, upload_info=None):
    doc = dict()
    if upload_info is not None:
        doc.update(upload_info)
    doc['created'] = datetime.now()
    doc['owner_id'] = owner
    doc['owner_type'] = owner_source
    doc['upload_path'] = upload_path
    insert_result = mongo['uploads'].insert(doc)
    return insert_result


def s3_bucket_connection():
    s3 = boto3.client('s3', aws_access_key_id=settings.AWS_SERVER_PUBLIC_KEY,
                      aws_secret_access_key=settings.AWS_SERVER_SECRET_KEY,
                      region_name=settings.REGION_NAME)
    return s3

def _save_new_csv_file(file_obj, path):
    s3_client = s3_bucket_connection()
    try:
        with open(file_obj, 'rb') as data:
            s3_client.upload_fileobj(data, settings.BUCKET_NAME, path)
        # s3_client.upload_fileobj(file_obj, settings.BUCKET_NAME, path)
    except Exception as e:
        LOG.debug(e)
        raise #UploadExists("File {} already exists.".format(filename))

def _save_new_uploaded_file(file_obj, path):
    s3_client = s3_bucket_connection()
    try:
        s3_client.upload_fileobj(file_obj, settings.BUCKET_NAME, path)
    except Exception as e:
        LOG.debug(e)
        raise #UploadExists("File {} already exists.".format(filename))

def _upload_file_direct(file_name, path):
    s3_client = s3_bucket_connection()
    try:
        response = s3_client.upload_file(file_name, settings.BUCKET_NAME, path)
    except Exception as e:
        return e
    return True

def create_refresh_token_for_api_user(api_user, days_till_expire=None):
    if days_till_expire is None:
        expires = datetime.timedelta(days=365)
    else:
        if not 0 < days_till_expire < 365:
            raise InvalidDaysValueForExpire("Value for days till expiration must be an int between 0 and 365")
        else:
            expires = datetime.timedelta(days=days_till_expire)
    token = create_refresh_token(identity=api_user, expires_delta=expires)
    expiration_date = datetime.datetime.now() + expires
    return token, expiration_date


def deduce_portals_from_user_groups(user_groups):
    portals = mongo['portals'].find(
        {"user_group_entry_urls.user_group": {"$in": user_groups}},
        {'name': 1, "user_group_entry_urls.$.entry_url": 1})

    resp = []
    for portal in portals:
        entry = dict()
        try:
            entry['portal_name'] = portal['name']
            entry['entry_url'] = portal['user_group_entry_urls'][0]['entry_url']
        except KeyError:
            continue
        resp.append(entry)
    LOG.debug("Allowed portals deduced: {}".format(resp))
    return resp


def find_api_user_by_email(email):
    api_user = mongo['api_users'].find_one({'email': email})
    return api_user


def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(context)


def lookup_user_in_collection(user_id, collection_name):
    user = mongo[collection_name].find_one({"_id": ObjectId(user_id)})
    return user


def create_api_user_from_user_in_collection(user_id, collection_name):
    user = lookup_user_in_collection(user_id, collection_name)
    already_present = mongo['api_users'].find({collection_name: ObjectId(user_id)})
    count = already_present.count()
    if count > 0:
        raise ReferencePresent("Reference for user {} from {} is present in api_user {}".format(
            user_id, collection_name, [present_in['_id'] for present_in in already_present]))
    email = user.get('email', None)
    if email is None:
        raise NoEmail('Email field is required for api user creation.')
    else:
        already_present = mongo['api_users'].find({"email": email})
        if already_present.count() > 0:
            raise ReferenceError("Duplicate e-mail {}".format(email))

    password = user.get('password', None)
    if password is None:
        raise ValueError('Password field is required for api user creation.')

    doc = {"email": email, collection_name: user['_id'], "password": password, "sources": [collection_name]}
    doc = mongo['api_users'].insert_one(doc)
    inserted_api_user_id = doc.inserted_id
    refresh_token, expiration_date = create_refresh_token_for_api_user({"_id": inserted_api_user_id})
    doc = mongo["api_users"].update_one(
        {"_id": inserted_api_user_id},
        {"$set": {"refresh_token": refresh_token, "token_expiration_date": expiration_date, "old_passwords": [], "login_attempt" : {
        "attempts" : 0,
        "blocked_at" : None,
        "created_at" : None
    },
    "password_updated_at" : datetime.now() }}
                                        )
    return inserted_api_user_id


def assign_user_group_to_api_user(user_oid, group_oid):
    group = mongo['user_groups'].find_one({"_id": group_oid})
    if group is None:
        raise ValueError("Group ID {} is invalid".format(group_oid))
    api_user = mongo['api_users'].find_one({"_id": user_oid})
    if api_user is None:
        raise ValueError("API_user ID {} is invalid".format(group_oid))
    groups = api_user.get('user_groups', [])
    if group in groups:
        return True
    else:
        groups.append(group_oid)
        result = mongo['api_users'].update_one({"_id": api_user["_id"]}, {"$set": {"user_groups": groups}})
        if result:
            return True
        else:
            return False


def _save_uploaded_file(file_obj, upload_path):
    filename = secure_filename(file_obj.filename)
    s3_client = s3_bucket_connection()
    path = os.path.join(upload_path, filename)
    try:
        s3_client.upload_fileobj(file_obj, settings.BUCKET_NAME, path)
        return path, filename
    except Exception as e:
        raise UploadExists("File {} already exists.".format(filename))


def make_upload_doc(owner, owner_source, file_obj, upload_path, upload_info):
    try:
        path, file_name = _save_uploaded_file(file_obj, upload_path)
    except UploadExists:
        file_name = secure_filename(file_obj.filename)
        path = os.path.join(upload_path, file_name)
        existing_upload = mongo['uploads'].find_one({'upload_path': upload_path})
        if existing_upload:
            mongo['uploads'].remove({"_id": existing_upload['_id']})
    upload_info['file_name'] = file_name
    inserted_id = _register_file_upload(owner, owner_source, upload_path=path, upload_info=upload_info)
    if inserted_id:
        return inserted_id
    return inserted_id


def check_protection(form_data):
    try:
        user_exists = mongo.api_users.find_one({"email": form_data.get("email")})
        if not user_exists:
            return "Combination of email address and password does not exist"
        if not form_data.get("password") or len(form_data.get("password")) < 6:
            return "Password should be atleast 6 characters"

        user =  APIMAsterUser(api_user=user_exists)

        temp_blocked = user.check_for_temp_blocked()

        if temp_blocked:
            return temp_blocked

        check_change_pass = user.check_for_change_pass()

        if check_change_pass:
            return check_change_pass

        if not check_password_hash(user.password, form_data.get("password")):
            return user.invalid_password_attempt()
        else:
            user.update_attempt_protection()
        return None

    except Exception as e:
        return e


def find_by_email_user_in_collection(email, collection):
    user = mongo[collection].find_one({'email': email})
    return user


def find_applicant_by_email_case_insensitive(email):
    email = re.escape(email)
    applicant = mongo['applicants'].find_one({'email': re.compile(email, re.IGNORECASE)})
    return applicant


def list_uploads_for_applicant(applicant_id, upload_type="all"):
    """
    :param applicant_id: <ObjectId()> - applicant's ID
    :param upload_type: <str> default: "all" - specific upload_type or "all"
    :return: list of upload documents satisfying criteria. May be empty
    """
    if upload_type == "all":
        uploads = mongo.uploads.find({'owner_id': applicant_id, 'is_deleted': {'$ne': True}})
    else:
        uploads = mongo.uploads.find({'owner_id': applicant_id, 'upload_type': upload_type, 'is_deleted': {'$ne': True}})
    return list(uploads)


def _mark_upload_deleted_by(upload_id, deleter_id, deleter_source='applicants'):
    """
    This function marks the upload object as deleted and also saves the
    deleter id in upload object.
    :param upload_id: <ObjectId()> - upload's ID
    :param deleter_id: <ObjectId()> - deleter's ID (applicants Id/clients Id/researcher Id)
    :param deleter_source:  <str()> - deleter type (applicant/client/researchers)
    :return: Boolean True/False
    """
    result = mongo.uploads.update(
        {'_id': upload_id},
        {'$set':
             {'is_deleted': True,
              'deleted': {'by': deleter_id,
                          'at': datetime.now(),
                          'by_source': deleter_source}
              }})
    return result['nModified'] == 1


def load_parameter_from_excel(filepath):
    """
    This method converts eccel to key: value format(dict())
    :param filepath: excel filepath to be loaded
    :return: All the data in key: value paire
    """
    wb = xlrd.open_workbook(filepath)
    sheet = wb.sheet_by_index(0)
    keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
    tuple_list = []
    for row_index in range(1, sheet.nrows):
        row_data = []
        for col_index in range(sheet.ncols):
            row_data.append(sheet.cell(row_index, col_index).value)
        row_data_tuple = tuple(row_data)
        tuple_list.append(row_data_tuple)
    return tuple_list


def create_notification(
        message_to, message_body, message_subject=None,
        message_from='vetty@support.vetty.co', dynamic_body=False,
        message_via='email', is_active=True, rec_settings=None, total_count=1,
        message_attachment_ids=None, **kwargs):
    """
    Creates recurring notifications DB entry
    :param message_to: <email/phone> required
    :param message_body: <str> required
    :param message_subject: <str> defaults to ''
    :param message_from: <email> defaults to 'support@smartscreen.tech'
    :param dynamic_body: <boolean> re-evaluate message body before dispatching? not implemented
    :param message_via: <str> defaults to 'email'. 'sms' - not implemented
    :param is_active: <boolean> defaults to True
    :param rec_settings: <{'period_value': <int>, 'period_units': <str plural>}>
    :param total_count: <int>
    :return: ObjectId of inserted record
    """

    if dynamic_body is True:
        raise NotImplementedError('Dynamic notification body not implemented yet')
    if message_via != 'email':
        raise NotImplementedError('Transporting with message_via {} is not implemented. Try using "email".'.format(message_via))

    if rec_settings is None:
        rec_settings = notifications.T.rec_settings.daily
    message = {
        'to': message_to,
        'from': message_from,
        'subject': message_subject if message_subject else "",
        'body': message_body,
        'dynamic_body': dynamic_body
    }
    if message_attachment_ids:
        message['attachment_ids'] = message_attachment_ids

    entry = {
        "message": message,
        "message_via": message_via,
        "is_active": is_active,
        "rec_settings": rec_settings,
        "remaining_count": total_count,
        "total_count": total_count,
        "last_ten": []
    }
    entry.update(kwargs)
    res = mongo['recurring_notifications'].insert_one(entry)
    return res.inserted_id