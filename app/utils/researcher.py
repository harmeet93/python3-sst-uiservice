

def applicant_alias_dict():
    return {
        "edver":['educationInfo'],
        "drivrec":['drivingLicenseInfo'],
        "Insuranceveri":['insurance_data'],
        # "credit_check":['ssn'],
        # "crimcounty":['dob','ssn_id','address','ssntrace','addresshistory'],
        # "drugs_10pan":['check_code', 'ssn'],
        # "drugs_10pan":['check_code'],
        # "drugs_9pan":['check_code'],
        # "drugs_7pan":['check_code'],
        # "drugs_5pan":['check_code'],
        "empver": ['employementInfo'],
        # "fedcrim":['state'],
        # "gstw":['national_criminal_report_ata'],
        # "sexof":['national_criminal_report_ata'],
        # "ssntrace":['name','dob','ssn_id'],
        # "statecrim":['state','ssn'],
        # "natcrim":['name','dob'],
    } 

def applicant_report_results_dict():
    return {
        "drivrec": ["drivinglicense"],
        "drugs_10pan": ["drugscreening"],
        "drugs_9pan": ["drugscreening"],
        "drugs_7pan": ["drugscreening"],
        "drugs_5pan": ["drugscreening"],
        "edver": ["educationverification"],
        "empver": ["employmentverification"],
        "gstw": ["terroristwl"],
        "natcrim": ["nationalcrim"]
    }

def applicant_research_results_dict():
    return  {
        "drivrec": ["drivinglicense"],
        "empver": ["employementInfo", "empver"],
        "drugs_10pan": ["drugs_10pan"],
        "drugs_9pan": ["drugs_9pan"],
        "drugs_7pan": ["drugs_7pan"],
        "drugs_5pan": ["drugs_5pan"],
        "edver": ["edver"],
        "licenseveri": ["license_data"],
        "gstw": ["terroristwl"],
        "natcrim": ["nationalcrim"],
        "Insuranceveri": ["insurance_data"],
    }


def applicant_humanized_results_dict():
    return {
        "drivrec": ["drivinglicense"],
        "drugs_10pan": ["drugs_10pan"],
        "drugs_9pan": ["drugs_9pan"],
        "drugs_7pan": ["drugs_7pan"],
        "drugs_5pan": ["drugs_5pan"],
        "edver": ["educationverification"],
        "empver": ["employmentverification"],
        "gstw": ["gstw"],
        "natcrim": ["nationalcrim"]
    }
