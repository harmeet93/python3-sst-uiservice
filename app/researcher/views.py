import flask
from app import smartscreen_api as api
from flask import request, make_response, jsonify, Response
import json
from app import mongo, LOG
from bson import json_util, ObjectId
import app.researcher.business as researcher_business
from app.jwt_auth import jwt_required, get_current_user, access_authorized
from app.main import JSONOIDEncoder
from app.main import make_response_custom_json_encoder, compare_time_passed
from datetime import datetime
from functools import wraps
from app.exceptions import NoRecordFound, CheckProtectionError
from flask import render_template
from app.jwt_auth import issue_access_token_for_api_user_id, jwt_required, issue_access_token_for_api_user, get_routes_ui_aliases_for_api_user, APIMAsterUser
from app.utils import app_business



@api.route("/admin/researcher/login", methods=["POST"])
def handle_researcher_login():
    """
        Handle the researcher login
    """

    try:
        api_user = researcher_business.researcher_login(request)
        if isinstance(api_user, (APIMAsterUser)):
            researcher = api_user.get_researcher_record()
            payload = {
                "id": str(researcher['_id']),
                "email": researcher['email'],
                "first_name": researcher['first_name'],
                "last_name": researcher['last_name'],
                "role": "researcher",
                "access_token": issue_access_token_for_api_user(api_user=api_user),
                "allowed_actions": get_routes_ui_aliases_for_api_user(api_user)
            }
            response = make_response(jsonify({"status": True, "message": payload }), 200)
        else:
            response = make_response(jsonify({"status": False, "message": api_user }), 401)
        return response    
    except Exception as e:
        LOG.exception("Error while login researcher {}".format(e))
        response = make_response(jsonify({"status": False, "message": "%s: %s" % (e.__class__.__name__, e)}))
    return response



@api.route('/docs', defaults={'filename': 'index.html'})
@api.route('/docs/<path:filename>')
def document(filename):
    try:
        return flask.send_from_directory(
            "../docs/build/html/",
            "index.html"
        )
    except Exception as e:
        print("error", e)
        raise e

@api.route("/researcher/order/<applicant_id>", methods=["GET"])
@jwt_required
def get_single_order_applicant(applicant_id):
    """Get single applicant details:
    """

    try:
        a = researcher_business.get_single_applicant(applicant_id)
        a = researcher_business.client_id_to_name(a)
        a = researcher_business.exclude_applicant_checks_for_researcher(a ,get_current_user().get_researcher_record())
        response = make_response(jsonify(a))
        resp_data = a
    except Exception as e:
        resp_data = {"status": False, "message": "%s: %s" % (e.__class__.__name__, e)}
        LOG.error(resp_data)
    finally:
        response = Response(json.dumps(resp_data, default=json_util.default), mimetype="application/json")
        response.headers['Content-Type'] = "application/json"
        return response


@api.route('/researcher/applicant/<applicant_id>/unpause_all', methods=["PATCH"])
@jwt_required
def unpause_all_paused_checks(applicant_id):
    """Unpause all checks of the applicant:
    """
    try:
        update_successful = researcher_business.unpause_all_checks(applicant_id)

        if update_successful:
            response = {"status": True, "message": "Document modified"}
        else:
            response = {"status": True, "message": "Document not modified"}

    except Exception as e:
        response = {"status": False, "message": "ResearcherAPI error {}: {}".format(e.__class__.__name__, e)}
    finally:
        return Response(json.dumps(response, default=json_util.default),
                    mimetype="application/json")



