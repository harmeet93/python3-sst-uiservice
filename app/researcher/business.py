from bson import ObjectId
from app import mongo
from app.utils import researcher_utils
from app.exceptions import NoRecordFound, CheckProtectionError
from app import LOG
from app.forms.login import ResearcherLogin, ApiUserLogin
from app.utils import app_business




def researcher_login(request):
    check = app_business.check_protection(request.form)
    if check:
        return check
    form = ResearcherLogin(request.form)
    if not form.validate():
        return str(list(form.errors.values())[0][0])
    else:
        api_user = form.user
    return api_user    


def apply_update_query_for_doc_in_collection(_id, collection_name, update, upsert=False, raise_not_found=False, raise_not_modified=False):
	if bool(update):
		res = mongo[collection_name].update_one({'_id': ObjectId(_id)}, update, upsert=upsert)
		if raise_not_found and res.matched_count == 0:
			raise DBRecordNotFound("Can't find {} in {} for update".format(_id, collection_name))
		if raise_not_modified and res.modified_count == 0:
			raise DBNotModified("Not modified {} of {} collection".format(_id, collection_name))
		return res
	return 0	


def applicant_allowed_for_researcher(applicant_id, researcher_record):
    try:
        if not 'allowed_clients' in researcher_record:
            return True
        if mongo.applicants.find_one({"_id": ObjectId(applicant_id), 'client_id': {'$in': researcher_record['allowed_clients']}}):
            return True
        else:
            return False
    except Exception as err:
        print (err)
        return False

def get_mongo_document_from_collection(document_id, collection_name, raise_not_found=False):
    found = mongo[collection_name].find_one({'_id': ObjectId(document_id)})
    if raise_not_found:
        if not found:
            raise DBRecordNotFound("Can't find {} in {}".format(document_id, collection_name))
    return found


def unpause_all_checks(applicant_strid):
	applicant = get_mongo_document_from_collection(applicant_strid, "applicants", raise_not_found=True)
	applicant_set = {}
	applicant_upd = {}
	for check_code, vendor_data in applicant.get('report_results', dict()).items():
		vendor_name = list(vendor_data.keys())[0]
		check_data = vendor_data[vendor_name]
		for num, so in enumerate(check_data.get('suborders', list())):
			if so.get('status') == 'paused':
				dotpath = "report_results.{}.{}.suborders.{}".format(check_code, vendor_name, num)
				applicant_set[dotpath] = "awaiting_vendor_information"
	if bool(applicant_set):
		applicant_upd = {'$set': applicant_set}
	res = apply_update_query_for_doc_in_collection(applicant['_id'], 'applicants', applicant_upd,raise_not_found=True)
	return 1

def lookup_user_in_collection(user_id, collection_name):
    user = mongo[collection_name].find_one({"_id": ObjectId(user_id)})
    return user


def get_single_applicant(applicant_id):
    applicant = mongo.applicants.find_one({
        "_id": ObjectId(applicant_id)
    })
    if not applicant:
        raise NoRecordFound("No record found")

    applicant['status'] = True
    applicant['id'] = str(applicant["_id"])
    applicant['created_by'] = get_client_created_by(applicant['client_user_id'])
    applicant['client_id'] = str(applicant['client_id'])
    applicant['client_user_id'] = str(applicant['client_user_id'])
    applicant['educationInfo'] = applicant['educationInfo'] if 'educationInfo' in applicant else None
    applicant['currentEmployerInfo'] = applicant['currentEmployerInfo'] if 'currentEmployerInfo' in applicant else None
    applicant['previousEmployerInfo'] = applicant['previousEmployerInfo'] if 'previousEmployerInfo' in applicant else None
    applicant['report_meta'] = applicant['report_meta'] if 'report_meta' in applicant else None
    applicant['profile_photo'] = applicant['profile_photo'] if 'profile_photo' in applicant else '/assets/images/avatar-m.png'


    applicant.pop('_id')
    applicant.pop('tasks', None)
    LOG.info("Single applicant data {}".format("applicant"))
    return applicant


def rebuilt_applicant_record_data(applicant_record, allowed_products):
    new_data = {}
    forbidden_checks_excluded = False
    for key, val in applicant_record.items():
        if not isinstance(val, dict):
            new_data[key] = val
            continue
        if key in allowed_products:
            new_data[key] = val
        elif not key in allowed_products:
            forbidden_checks_excluded = True

    return new_data, forbidden_checks_excluded


def get_client_created_by(client_id):
        client_users = mongo.client_user.find({"_id": client_id}, {'first_name': 1, 'last_name': 1, 'email': 1})
        cu_map = {c['_id']: c for c in client_users}
        a = {}
        if client_users is not None:
            a['first_name'] = cu_map[client_id]['first_name']
            a['last_name'] = cu_map[client_id]['last_name']
            a['email'] = cu_map[client_id]['email']
        else:
            a['first_name'] = None
            a['last_name'] = None
            a['email'] = None
        return a

def client_id_to_name(applicant):
    cid = applicant.get('client_id')
    cuid = applicant.get('client_user_id')
    if cid:
        client = lookup_user_in_collection(cid, "clients")
        applicant['client'] = dict(
            client_name=client.get('client_name'),
            contact_first_name=client.get('contact_first_name'),
            contact_last_name=client.get('contact_last_name'),
            contact_email=client.get('contact_email'),
            contact_phone=client.get('contact_phone'),
            owner_email=client.get('owner_email'),
            owner_name=client.get('owner_name'),
            owner_phone=client.get('owner_phone'),
        )
    if cuid:
        cu = lookup_user_in_collection(cuid, "client_user")
        applicant['client_user'] = dict(
            first_name=cu.get('first_name'),
            last_name=cu.get('last_name'),
            email=cu.get('email'),
            mobile_number=cu.get('mobile_number')
        )

    return applicant

def filter_applicant_data_for_alias(applicant_record, aliases, allowed_products):
    set_data = []
    forbidden_checks_excluded = False
    for k, v in aliases:
        for i in v:
            if (k in allowed_products):
                set_data.append(i)

            elif (i in set_data):
                continue    
            else:
                forbidden_checks_excluded = True
                applicant_record.pop(i, None)
    return applicant_record, forbidden_checks_excluded


def apply_aliases(allowed_products, aliases):
        applied_aliases = list()
        for code in allowed_products:
            if code in aliases:
                applied_aliases += aliases[code]
            else:
                applied_aliases.append(code)
        return applied_aliases


def exclude_applicant_checks_for_researcher(applicant_record, researcher_record):
    allowed_products = researcher_record.get('allowed_products', [])
    forbidden_checks_excluded = False

    applicant_record, excluded = filter_applicant_data_for_alias(applicant_record, researcher_utils.applicant_alias_dict().items(), allowed_products)
    if excluded:
        forbidden_checks_excluded = True

    if applicant_record.get('report_results'):
        aliases = researcher_utils.applicant_report_results_dict()
        aliases_applied = apply_aliases(allowed_products, aliases)

        applicant_record['report_results'], excluded = rebuilt_applicant_record_data(applicant_record['report_results'], aliases_applied)
        if excluded:
            forbidden_checks_excluded = True


    if applicant_record.get('research_results'):
        aliases = researcher_utils.applicant_research_results_dict()
        aliases_applied = apply_aliases(allowed_products, aliases)

        applicant_record['research_results'], excluded = rebuilt_applicant_record_data(applicant_record['research_results'], aliases_applied)
        if excluded:
            forbidden_checks_excluded = True

    if applicant_record.get('humanized_results'):
        aliases = researcher_utils.applicant_humanized_results_dict()
        aliases_applied = apply_aliases(allowed_products, aliases)
        applicant_record['humanized_results'], excluded = rebuilt_applicant_record_data(applicant_record['humanized_results'], aliases_applied)
        if excluded:
            forbidden_checks_excluded = True

    old_checklist = applicant_record['package'].get('checklist')
    if old_checklist:
        new_checklist = []
        for entry in old_checklist:
            if entry['code'] in allowed_products:
                new_checklist.append(entry)
        if len(old_checklist) != len(new_checklist):
            forbidden_checks_excluded = True
        applicant_record['package']['checklist'] = new_checklist

    applicant_record['forbidden_checks_excluded'] = forbidden_checks_excluded
    LOG.info("Applicant after removing checks {}".format("applicant_record") )
    return applicant_record

