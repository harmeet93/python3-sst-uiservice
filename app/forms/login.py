"""the admin login form
"""

# from flask_wtf import Form
# from wtforms import PasswordField
# from wtforms.fields.html5 import EmailField
# from werkzeug.security import check_password_hash
# from wtforms import validators
# from app import mongo
# from app.jwt_auth import APIMAsterUser
# from app.forms.user import User

from flask_wtf import FlaskForm, RecaptchaField
from wtforms import PasswordField, StringField
from wtforms.fields.html5 import EmailField
# from werkzeug.security import check_password_hash
from wtforms import validators
from app import mongo
from .user import User
# import settings
from wtforms.validators import Required
from flask_wtf import recaptcha
from flask import request
# from werkzeug.security.check import check_password_hash,generate_password_hash
from werkzeug.security import check_password_hash, generate_password_hash
from app.jwt_auth import create_refresh_token_for_api_user, APIMAsterUser



class AdminLogin(FlaskForm):
    """The basic login FlaskForm
    """
    def get_user(self, email):
        """returns the requested user
        :param email: to look up client by email
        """
        user_exists = mongo.admin_user.find_one({"email":email})
        if user_exists:
            return User(user_exists)
        else:
            return None

    def validate_email(self, field):
        """validates the user's supplied email
        :param field: the field to be vlaidated, contains user email
        """
        self.user = self.get_user(field.data)
        if not self.user:
            raise validators.ValidationError("Combination of email address and password does not exist")
        if not self.user.is_active():
            self.is_activated = False
            raise validators.ValidationError("Your account is suspended")

    def validate_password(self, field):
        """validates the user's supplied password

        :param field: the field to be validated, contains password
        :return:
        """
        if not self.user:
            raise validators.ValidationError("Invalid user")
        if not check_password_hash(self.user.password, field.data):
            raise validators.ValidationError("Combination of email address and password does not exist")

    user = None
    is_activated = None
    email = EmailField("email address", [validators.DataRequired()])
    password = PasswordField("password", [validators.DataRequired()])



class ResearcherLogin(FlaskForm):
    """The basic login FlaskForm
    """
    def get_user(self, email):
        """returns the requested user
        :param email: to look up client by email
        """
        user_exists = mongo.api_users.find_one({"email": email})
        if user_exists:
            return APIMAsterUser(api_user=user_exists)

        else:
            return None

    def validate_email(self, field):
        """validates the user's supplied email
        :param field: the field to be vlaidated, contains user email
        """
        self.user = self.get_user(field.data)
        if not self.user:
            raise validators.ValidationError("Combination of email address and password does not exist")
        # if not self.user.is_active():
        #     self.is_activated = False
        #     raise validators.ValidationError("please verify your email")

    def validate_password(self, field):
        """validates the user's supplied password

        :param field: the field to be validated, contains password
        :return:
        """
        if not self.user:
            raise validators.ValidationError("Combination of email address and password does not exist")
        if not check_password_hash(self.user.password, field.data):
            raise validators.ValidationError("Combination of email address and password does not exist")

    user = None
    #is_activated = None
    email = EmailField("email address", [validators.DataRequired()])
    password = PasswordField("password", [validators.DataRequired()])


class ClientLogin(FlaskForm):
    """The basic login FlaskForm
    """
    def get_user(self, email):
        """returns the requested user
        :param email: to look up client by email
        """
        user_exists = mongo.client_user.find_one({"email":email})
        if user_exists:
            return User(user_exists)
        else:
            return None

    def validate_email(self, field):
        """validates the user's supplied email
        :param field: the field to be vlaidated, contains user email
        """
        self.user = self.get_user(field.data)
        if not self.user:
            raise validators.ValidationError("Combination of email address and password does not exist")
        if not self.user.is_active():
            self.is_activated = False
            raise validators.ValidationError("Your account is suspended")
        if self.user.suspended is True:
            raise validators.ValidationError("Your account is suspended")

    def validate_password(self, field):
        """validates the user's supplied password

        :param field: the field to be validated, contains password
        :return:
        """
        if not self.user:
            raise validators.ValidationError("Combination of email address and password does not exist")
        if not check_password_hash(self.user.password, field.data):
            raise validators.ValidationError("Combination of email address and password does not exist")

    user = None
    is_activated = None
    email = EmailField("email address", [validators.DataRequired()])
    password = PasswordField("password", [validators.DataRequired()])


class ApiUserLogin(ClientLogin):
    def get_user(self, email):
        try:
            user_exists = mongo.api_users.find_one({"email": email})
            if user_exists:
                u = User(user_exists)
                u.activated = True
                return u
            else:
                return None
        except Exception as e:
            print("not found api user")
            raise e




class ForgotPassword(FlaskForm):
    """client signup FlaskForm`
    """
    def validate_email(self, field):
        """check if user by this email does not already exists
        """
        user = mongo.client_user.find_one({
            "email": field.data
        })
        if not user:
            raise validators.ValidationError("Combination of email address and password does not exist")

    email = EmailField("email")
    captcha = RecaptchaField()
