""" user class as needed by flask-login
"""

class User():
	def __init__(self, user):
		self.user = user
		self._id = user["_id"] if user and "_id" in user else None
		self.updated = user['updated'] if user and 'updated' in user else None
		self.last_name = user['last_name'] if user and 'last_name' in user else None
		self.created = user["created"] if user and 'created' in user else None
		self.client_id = user["client_id"] if user and 'client_id' in user else None
		self.email = user["email"] if user and 'email' in user else None
		self.first_name = user["first_name"] if user and 'first_name' in user else None
		self.password = user["password"] if user and 'password' in user else None
		self.picture = user['picture'] if user and 'picture' in user else None
		self.add_users = user["add_users"] if user and 'add_users' in user else False
		self.add_applicants = user["add_applicants"] if user and 'add_applicants' in user else False
		self.mobile_number = user['mobile_number'] if user and 'mobile_number' in user else None
		self.mobile_verified = user['mobile_verified'] if user and 'mobile_verified' in user else None
		self.activated = user['activated'] if user and 'activated' in user else None
		self.suspended = user['suspended'] if user and 'suspended' in user else None

	def is_authenticated(self):
		return True

	def is_active(self):
		return self.activated

	def is_anonymous(self):
		return False

	def get_id(self):
		return str(self.user["_id"])

	# def is_suspended(self):
	# 	return self.suspended
