from app import smartscreen_api, LOG
import json
from werkzeug.wrappers import Request


@smartscreen_api.after_request
def after_request_func(response):
    try:
        resp_data = { "is_response" : True, "response" : json.loads(response.data)}
        LOG.info(resp_data)
    except Exception as e:
        resp_data = { "is_response" : True, "response" : "error in response", "error" : str(e)}
        LOG.info(resp_data)
    return response


class Middleware(object):

    def __init__(self, app):
    	self.app = app
    	after_request_func

    def __call__(self, environ, start_response):
        return self.app(environ, start_response)
