import pytest
import app.utils.app_business as APPBS
from bson import ObjectId


@pytest.mark.parametrize("input, results", [({"applicant_id": "5ba489fa7a13cab6881f2e9b",
                                                "upload_id": "5c136ef2bce1db1b6348c644", "deleter": "applicants"}, True)])
def test_mark_upload_deleted_by_applicant(input, results):
    """
    This test method test's whether upload id is marked deleted.
    method: _mark_upload_deleted_by(upload_id, deleter_id, deleter_source=deleter_source)
    values: deleter_id, upload_id, deleter_source
    response: boolean True/False
    """
    deleter_id = ObjectId(input.get("applicant_id"))
    upload_id = ObjectId(input.get("upload_id"))
    deleter_source = input.get("deleter")
    res_uploads = APPBS._mark_upload_deleted_by(upload_id, deleter_id, deleter_source=deleter_source)
    assert res_uploads == results
