import pytest
import app.applicant.business as APPBS
from bson import ObjectId


@pytest.mark.parametrize("input, results", [({"applicant_id": "5dea785f1e952c3d0166be56"}, True)])
def test_remove_middle_name(input, results):
    """
    This method test whether there is an entry for comment in applicant object
    method: remove_applicant_middle_name(applicant_id)
    values: applicant_id
    response: boolean True/False
    """
    applicant_id = ObjectId(input.get("applicant_id"))
    res = APPBS.remove_applicant_middle_name(applicant_id)
    assert res == results
