import pytest
import app.applicant.business as APPBS
from app.exceptions import ApplicantMissingField


@pytest.mark.parametrize("input, expected_result", [({'applicant_id': '5a1289b8453d970e1440e387', 'upload_id': '5e4a93785cd7ce1ae1f0cca8'}, (["_id" ,"upload_type" ,"file_name" ,"file_name_short" ,"created" ,"owner_id" ,"owner_type" ,"upload_path" ]) ) ] )
def test_generate_signed_sa_success(input, expected_result):
    """
    method: generate_signed_sa(params)
    params: dict()
    values: list
    response: signed document list
    """
    res = APPBS.generate_signed_sa(input.get("applicant_id"), input.get("upload_id"))
    assert list(res[0].keys()) == expected_result


@pytest.mark.parametrize("input, expected_result", [({'applicant_id': '5c173bb942bde31637e0a306', 'upload_id': '5e4d40e0e40043127c33fa10'}, (["_id" ,"upload_type" ,"file_name" ,"file_name_short" ,"created" ,"owner_id" ,"owner_type" ,"upload_path" ]) ) ] )
def test_generate_signed_sa_error(input, expected_result):
    """
    method: generate_signed_sa(params)
    params: dict()
    values: list
    response: error
    """
    with pytest.raises(ApplicantMissingField, match="digital_signature"):
        assert APPBS.generate_signed_sa(input.get("applicant_id"), input.get("upload_id"))

