import pytest
import app.applicant.business as APPBS
from app.exceptions import ApplicantUploadNotFound
from bson import ObjectId


def compare_upload_object_keys(list_upload_object, results):
    check = False
    for upload in list_upload_object:
        check = all(key in list(upload.keys()) for key in results)
        if not check:
            break
    return check


@pytest.mark.parametrize("input", [({"applicant_id": "5adda8f4a3ca9f0c7aa581fe", "upload_type": "all"})])
def test_get_uploads_object_key_data(input):
    """
    method: get_uploads_object(params)
    params: dict()
    values: applicant_id, upload_type
    response: upload_object
    """
    results = ['_id', 'upload_path', 'owner_type', 'owner_id', 'upload_url']
    applicant_id = ObjectId(input.get("applicant_id"))
    upload_type = input.get("upload_type")
    res_uploads = APPBS.get_uploads_object(applicant_id, upload_type=upload_type)
    assert compare_upload_object_keys(res_uploads, results) == True


@pytest.mark.parametrize("input", [({"applicant_id": "5ba489fa7a13cab6881f2e9b", "upload_type": "donor_pass"})])
def test_get_uploads_object_key_exception(input):
    """
    method: get_uploads_object(params)
    params: dict()
    values: applicant_id, upload_type
    response: upload_object
    """
    with pytest.raises(ApplicantUploadNotFound, match="Uploads of type '{}' not found.".format(input.get("upload_type"))):
        applicant_id = ObjectId(input.get("applicant_id"))
        upload_type = input.get("upload_type")
        assert APPBS.get_uploads_object(applicant_id, upload_type=upload_type)
