import pytest
import app.applicant.business as APPBS


@pytest.mark.parametrize("input, expected_result", [({'_id': '5e45b2385cd7ce1e617468a2', 'file_name': 'insurance_data_VijayLast_olsNew_C-01-2020.pdf', 'upload_path': 'static/applicant_docs/5a1289b8453d970e1440e387/insurance_data/insurance_data_VijayLast_olsNew_C-01-2020.pdf'}, (["_id" , "upload_url" , "file_name"]) ) ] )
def test_make_uploaded_data_success(input, expected_result):
    """
    method: make_uploaded_data(params)
    params: dict()
    values: _id, upload_url, file_name
    response: uploaded_data_object
    """
    res = APPBS.make_uploaded_data(input)
    assert list(res.keys()) == expected_result


@pytest.mark.parametrize("input, expected_result", [(None, (["_id" , "upload_url" , "file_name"]) ) ] )
def test_make_uploaded_data_exception(input, expected_result):
    """
    method: make_uploaded_data(params)
    params: dict()
    values: _id, upload_url, file_name
    response: assert
    """

    with pytest.raises(Exception):
        res = APPBS.make_uploaded_data(input)
        print(res)
        assert list(res.keys()) == expected_result
    # res = APPBS.make_uploaded_data(input)
    # assert list(res.keys()) == expected_result


@pytest.mark.parametrize("input, expected_result", [ ({'section': 'insurance_data', 'submit_later': 'false', 'case': '0', 'applicant_id': '5a1289b8453d970e1440e387', 'replace_all_existing': 'true'}  , (["file" , "record" , "file_wiped", "record_wiped"]) ) ] )
def test_wipe_uploads_if_needed_success(input, expected_result):
    """
    method: wipe_uploads_if_needed(params, applicant_id, upload_type)
    params: dict()
    values: _id, upload_url, file_name
    response: uploaded_data_object
    """
    applicant_id = input.get("applicant_id")
    upload_type = input.get("section")
    res = APPBS.wipe_uploads_if_needed(input, applicant_id, upload_type)
    if res:
        assert list(res.keys()) == expected_result
    assert res == []
