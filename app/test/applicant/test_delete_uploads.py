import pytest
import app.applicant.business as APPBS
from app.exceptions import ApplicantUploadNotFound
from bson import ObjectId


@pytest.mark.parametrize("input, results", [({"applicant_id": "5ba489fa7a13cab6881f2e9b",
                                                "upload_id": "5c136ef2bce1db1b6348c644"}, True)])
def test_delete_own_upload_applicant_deleted(input, results):
    """
    This test method test's whether upload id is marked deleted.
    method: delete_own_upload_applicant(applicant_id, upload_strid)
    values: applicant_id, upload_id
    response: boolean True/False
    """
    applicant_id = ObjectId(input.get("applicant_id"))
    upload_id = input.get("upload_id")
    res_uploads = APPBS.delete_own_upload_applicant(applicant_id, upload_id)
    assert res_uploads == results


@pytest.mark.parametrize("input, results", [({"applicant_id": "5ba489fa7a13cab6881f2e9b",
                                                "upload_id": "5c136ef2bce1db1b6348c643"}, True)])
def test_delete_own_upload_applicant_exception(input, results):
    """
    This test method test's whether the function delete_own_upload_applicant
    raise ApplicantUploadNotFound error
    method: delete_own_upload_applicant(applicant_id, upload_strid)
    values: applicant_id, upload_id
    response: boolean True/False
    """
    with pytest.raises(ApplicantUploadNotFound, match="No upload {} registered for applicant {}".format(
            input.get('upload_id'), input.get('applicant_id'))):
        applicant_id = ObjectId(input.get("applicant_id"))
        upload_id = input.get("upload_id")
        assert APPBS.delete_own_upload_applicant(applicant_id, upload_id)
