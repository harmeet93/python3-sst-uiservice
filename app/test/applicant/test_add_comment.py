import pytest
import app.applicant.business as APPBS
from app.exceptions import APIRequestFieldMissing, ApplicantLookupError
from bson import ObjectId


@pytest.mark.parametrize("input, results", [({"applicant_id": "5dea785f1e952c3d0166be56",
                                                "check_code": "drivinglicense", "case_id": "drivinglicense-0"}, True)])
def test_applicant_add_claim(input, results):
    """
    This method test whether there is an entry for comment in applicant object
    method: applicant_add_claim(applicant_id, claim_type, request_data)
    values: applicant_id, claim_type, request_data
    response: boolean True/False
    """
    applicant_id = ObjectId(input.get("applicant_id"))
    claim_type = "comment"
    request_data = input
    res_uploads = APPBS.applicant_add_claim(applicant_id, claim_type, request_data)
    assert res_uploads == results


@pytest.mark.parametrize("input, results", [({"applicant_id": "5dea785f1e952c3d0166be56",
                                              "case_id": "drivinglicense-0"}, True)])
def test_add_claim_APIRequestFieldMissing_exception(input, results):
    """
    This method test whether there is an exception raised for APIRequestFieldMissing
    method: applicant_add_claim(applicant_id, claim_type, request_data)
    values: applicant_id, claim_type, request_data
    response: boolean True/False
    """
    claim_type = "comment"
    with pytest.raises(APIRequestFieldMissing, match="API request parameter for creating {} are missing".format(
            claim_type)):
        applicant_id = ObjectId(input.get("applicant_id"))
        request_data = input
        assert APPBS.applicant_add_claim(applicant_id, claim_type, request_data)


@pytest.mark.parametrize("input, results", [({"applicant_id": "5dea785f1e952c3d0166be55",
                                              "check_code": "drivinglicense", "case_id": "drivinglicense-0"}, True)])
def test_add_claim_ApplicantLookupError_exception(input, results):
    """
    This method test whether there is an exception raised for ApplicantLookupError
    method: applicant_add_claim(applicant_id, claim_type, request_data)
    values: applicant_id, claim_type, request_data
    response: boolean True/False
    """
    claim_type = "comment"
    with pytest.raises(ApplicantLookupError, match="Applicant record not found"):
        applicant_id = ObjectId(input.get("applicant_id"))
        request_data = input
        assert APPBS.applicant_add_claim(applicant_id, claim_type, request_data)