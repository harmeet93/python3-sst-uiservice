import pytest
import app.client.business as CLBS
from bson import ObjectId
from bson.errors import InvalidId





@pytest.mark.parametrize("input, expected_result", [( {'client_user_id': '58884ff19c5d396319ef9c0a', 'client_id': ObjectId('58884ff19c5d396319ef9c09'), "data" : {"role": "Standard"} } , True ) ] )
def test_change_clients_role_success(input, expected_result):
    """
    method: change_clients_role(params)
    params: dict()
    values: client_user_id , client_id , dict()
    response: True
    """

    res = CLBS.change_clients_role(input)
    assert res == expected_result


@pytest.mark.parametrize("input, expected_result", [( {'client_user_id': '58884ff19c5d396319ef9c0p', 'client_id': ObjectId('58884ff19c5d396319ef9c09'), "data" : {"role": "Standard"} } , True ) ] )
def test_change_clients_role_error(input, expected_result):
    """
    method: change_clients_role(params)
    params: dict()
    values: client_user_id , client_id , dict()
    response: Exception (InvalidId)
    """

    with pytest.raises(InvalidId, match="'{}' is not a valid ObjectId, it must be a 12-byte input or a 24-character hex string".format(input.get('client_user_id'))):
        assert CLBS.activate_clients_user(input)






