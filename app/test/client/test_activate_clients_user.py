import pytest
import app.client.business as CLBS
from bson import ObjectId
from bson.errors import InvalidId





@pytest.mark.parametrize("input, expected_result", [( {'client_user_id': '58884ff19c5d396319ef9c0a', 'client_id': ObjectId('58884ff19c5d396319ef9c09')} , True ) ] )
def test_activate_clients_user_success(input, expected_result):
    """
    method: activate_clients_user(params)
    params: dict()
    values: client_user_id , client_id
    response: True or Exception
    """

    res = CLBS.activate_clients_user(input)
    assert res == expected_result


@pytest.mark.parametrize("input, expected_result", [( {'client_user_id': '5968e03de406d34d62ae179n', 'client_id':ObjectId("58e92e0b9c5d3907f9636a7e")} , True ) ] )
def test_activate_clients_user_error(input, expected_result):
    """
    method: activate_clients_user(params)
    params: dict()
    values: client_user_id , client_id
    response: Exception (InvalidId)
    """

    with pytest.raises(InvalidId, match="'{}' is not a valid ObjectId, it must be a 12-byte input or a 24-character hex string".format(input.get('client_user_id'))):
        assert CLBS.activate_clients_user(input)






