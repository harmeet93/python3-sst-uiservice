import pytest
import app.client.business as CLBS


@pytest.mark.parametrize("input, expected_result", [( "58884ff19c5d396319ef9c09"  , 0 ) ] )
def test_get_notifications_success(input, expected_result):
    """
    method: make_uploaded_data(params)
    params: dict()
    values: client id String ID
    response: list of notifications
    """
    res = CLBS.get_notifications(input)
    res = len(res)
    assert res > expected_result


# @pytest.mark.parametrize("input, expected_result", [( "58884ff19c5d396319ef9c09"  , 0 ) ] )
# def test_get_notifications_error(input, expected_result):
#     """
#     method: make_uploaded_data(params)
#     params: dict()
#     values: client id String ID
#     response: empty list
#     """

#     # with pytest.raises([]):
# 	res = CLBS.get_notifications(input)
# 	res = len(res)
# 	assert res == expected_result
