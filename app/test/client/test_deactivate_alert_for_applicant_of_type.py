from app import smartscreen_api as api
import pytest
import app.client.business as CLBS
from app.exceptions import MissingRequestParam
import app.notifications_api.business as notification_api_business



@pytest.mark.parametrize("input, expected_result", [( {'client_user_id': '5a1289b8453d970e1440e387','applicant_strid': '58884ff19c5d396319ef9c0a','alert_type': 'application_not_started' }  , True ) ] )
def test_deactivate_alert_for_applicant_of_type_success(input, expected_result):
    """
    method: deactivate_alert_for_applicant_of_type (params)
    params: client_user_id, applicant_strid, alert_type
    values: client_user_id, applicant_strid, alert_type 
    response: True or False
    """

    client_user_id = input.get("client_user_id")
    applicant_strid = input.get("applicant_strid")
    alert_type = input.get("alert_type")
    res = CLBS.deactivate_alert_for_applicant_of_type(client_user_id, applicant_strid, alert_type)
    if res:
        assert res == expected_result    
    assert res == False


@pytest.mark.parametrize("input, expected_result", [( {'client_user_id': '5a1289b8453d970e1440e387','applicant_strid': '58884ff19c5d396319ef9c0a','alert_type': None }  , True ) ] )
def test_deactivate_alert_for_applicant_of_type_success(input, expected_result):
    """
    method: deactivate_alert_for_applicant_of_type (params)
    params: client_user_id, applicant_strid, alert_type
    values: client_user_id, applicant_strid, alert_type 
    response: True or False
    """

    client_user_id = input.get("client_user_id")
    applicant_strid = input.get("applicant_strid")
    alert_type = input.get("alert_type")

    with pytest.raises(MissingRequestParam, match="No alert_type to disable specified"):
        assert CLBS.deactivate_alert_for_applicant_of_type(client_user_id, applicant_strid, alert_type)




