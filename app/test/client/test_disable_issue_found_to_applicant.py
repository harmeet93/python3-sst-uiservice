import pytest
import app.client.business as CLBS
from bson import ObjectId
from bson.errors import InvalidId


@pytest.mark.parametrize("input, expected_result", [( "5a1289b8453d970e1440e387" , True ) ] )
def test_applicant_disable_issue_found_success(input, expected_result):
    """
    method: applicant_disable_issue_found(params)
    params: str()
    values: applicant_id
    response: True
    """

    res = CLBS.applicant_disable_issue_found(input)
    assert res == expected_result


@pytest.mark.parametrize("input, expected_result", [( "5a1289b8453d970e1440e38qq" , True ) ] )
def test_activate_clients_user_error(input, expected_result):
    """
    method: applicant_disable_issue_found(params)
    params: str()
    values: applicant_id
    response: True
    """

    with pytest.raises(InvalidId, match="'{}' is not a valid ObjectId, it must be a 12-byte input or a 24-character hex string".format(input)):
        assert CLBS.applicant_disable_issue_found(input)


@pytest.mark.parametrize("input, expected_result", [( "5a1289b8453d970e1440e388" , "No applicant found, cannot update" ) ] )
def test_activate_clients_user_error(input, expected_result):
    """
    method: applicant_disable_issue_found(params)
    params: str()
    values: applicant_id
    response: True
    """

    res =  CLBS.applicant_disable_issue_found(input)
    assert res == "No applicant found, cannot update"

