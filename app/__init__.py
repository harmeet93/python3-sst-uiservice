"""smartscreen restful api
"""
import os
import sys
from flask import Flask, render_template
from flask_pymongo import PyMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import settings
import mongo_proxy
import tempfile
from flask_cors import CORS, cross_origin
import request
from settings import ENV
from app.log import LOG

smartscreen_api = Flask(__name__, template_folder="../templates", static_url_path="/", static_folder='../docs/build/html/')
CORS(smartscreen_api)

env = os.environ.get("SMARTSCREEN_ENVIRON", "dev")
smartscreen_api.config.from_pyfile("../config/%s.py" % env)

mongo = MongoClient(settings.MONGO_URI, socketKeepAlive=True, socketTimeoutMS=300000)
mongo = mongo[settings.MONGO_DBNAME]


from app.admin import views
from app.client import views
from app.applicant import views
from app.researcher import views


print(ENV)
LOG.debug("SST-uiservice application started with settings environment ENV {}".format(ENV))
from app.middleware import Middleware
smartscreen_api.wsgi_app = middleware.Middleware(smartscreen_api.wsgi_app)
print(ENV)

