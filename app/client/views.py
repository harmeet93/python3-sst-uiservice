import flask
from app import smartscreen_api as api
from flask import request, make_response, jsonify, Response
import json
from app import mongo, LOG
from bson import json_util, ObjectId
import app.client.business as client_business
from app.jwt_auth import jwt_required, get_current_user, access_authorized
from app.main import JSONOIDEncoder
from app.main import make_response_custom_json_encoder, compare_time_passed
from datetime import datetime
from functools import wraps
from app.utils import app_business
from app.exceptions import ClientAPIV3Exception
from bson.errors import InvalidId


@api.route("/login", methods=["POST"])
@api.route("/partial_login", methods=["POST"])
def handle_partial_login():
    """Handles client login and resolves permissions
    Additionally returns list of permitted products
    """
    try:
        api_user = client_business.client_login(request)
        if isinstance(api_user, (dict)):
            response = make_response(jsonify({"status": True, "message": api_user }), 200)
        else:
            response = make_response(jsonify({"status": False, "message": api_user }), 401)
                
        return response    
    except Exception as e:
        response = make_response(jsonify({"status": False, "message": "%s: %s" % (e.__class__.__name__, e)}))
        LOG.exception("Error while login client {}".format(e))
    return response


@api.route("/v3/clients/info", methods=["GET"])
@jwt_required
def get_clients_data(**kwargs):
    """
        Gets the consent info of the applicant
    """

    api_user = get_current_user()
    client_user_id = str(api_user['client_user'])
    client_user = client_business.lookup_user_in_collection(client_user_id, 'client_user')
    client_id = client_user['client_id']
    response = client_business.get_clients_data({
            "client_id": client_id,
            "client_user_id":client_user_id
        })
    return Response(json.dumps(response, default=json_util.default), mimetype="application/json")


@api.route("/v3/client/notifications", methods=['GET'])
@jwt_required
def show_notification():
    """
    This Endpoint return the 10 nofications if database having
    """
    api_user = get_current_user()
    client_id = api_user.get_client_users_client_id()
    print("hehehe ", client_id)
    response = client_business.get_notifications(client_id)

    if not response:
        response = {"status":  False, "message": "no notifications"}
    return make_response_custom_json_encoder(response)




@api.route("/v3/client/actions/<applicant_strid>/mail_to", methods=['PUT'])
@jwt_required
def action_mail_to_applicant(applicant_strid):
    """
    This endpoint send the mail to the applicant with alert_type and also disable the alert with the argument passings from request
    """
    try:
        api_user = get_current_user()
        client_user_id = api_user.get_client_user_id()
        json_data = request.get_json()
        action_log = dict()
        action_log['email_sent'] = client_business.client_send_applicant_email(client_user_id, applicant_strid, json_data)

        deactivate_alert = request.args.get('disable_alert')
        alert_type = request.args.get('alert_type')
        if deactivate_alert == 'true':
            action_log['alert_deactivated'] = client_business.deactivate_alert_for_applicant_of_type(client_user_id, applicant_strid, alert_type)

        response = { 'status': True, 'message': "API call success", 'action_log': action_log }
    except ClientAPIV3Exception as e:
        LOG.exception("error in ClientAPIV3Exception" + str(e))
        response = {"status": False, "message": "ClientAPIV3 error: " + str(e)}
    except Exception as e:
        LOG.exception("error in Exception" + str(e))
        response = {"status":  False, "message": str(e)}
    return make_response_custom_json_encoder(response)



@api.route("/v3/clients_user/<client_user_id>", methods=['PUT'])
@jwt_required
def activate_clientuser(client_user_id):
    """
        Client API to activate client
    """
    try:
        client_id = get_current_user().get_client_users_client_id()
        response = client_business.activate_clients_user({ 'client_user_id': client_user_id, 'client_id': client_id })
        if response:
            response = {"status":  True, "message": "Client is activated successfully" }
    except InvalidId as e:
        LOG.exception("Error in activate client API", str(e))
        response = {"status":  False, "message": str(e)}
    except Exception as e:
        LOG.exception("Error in activate client API", str(e))
        response = {"status":  False, "message": str(e)}
    return Response(json.dumps(response, default=json_util.default ), mimetype="application/json")



@api.route("/v3/clients_user_role/<client_user_id>", methods=['PUT'])
@jwt_required
def update_clientuser_role(client_user_id):
    """
        Client API to change a client_user role
    """
    try:
        json_data = request.get_json(force=True)
        client_id = get_current_user().get_client_users_client_id()
        response = client_business.change_clients_role({ 'client_user_id': client_user_id, 'client_id': client_id, 'data': json_data })
        if response:
            response = {"status":  True, "message": "Client role is changed successfully" }
    except InvalidId as e:
        LOG.exception("Error in activate client API InvalidId", str(e))
        response = {"status":  False, "message": str(e)}
    except Exception as e:
        LOG.exception("error in update clientuser role", str(e))
        response = {"status":  False, "message": str(e)}
    return Response(json.dumps(response, default=json_util.default), mimetype="application/json")




@api.route("/v3/client/documents", methods=["GET"])
@jwt_required
def list_docs_for_client():
    """
   This Endpoint excepts page, results as query paramters for limit and offset.
   It will find the list of docs and applicant object corresponding to that doc for the particular client.
    """
    try:
        api_user = get_current_user()
        #client_user_id = api_user.get_client_user_id()
        client_id = api_user.get_client_users_client_id()
        query={}
        query,queryDoc = client_business.generate_docs_filter_query(request.args)
        query['client_id'] = client_id
        limit, offset = client_business.get_pagination_args(request.args)
        docs, total = client_business.get_paginated_applicants_documents_with_query(query,queryDoc,limit, offset)

        response = {"status":  True,
                    "message": "success",
                    "docs": docs,
                    # "applicants": applicants_list,
                    "total":      total

                    }

    except Exception as e:
        response = {"status":  False, "message": e}
    return make_response_custom_json_encoder(response)



@api.route("/v3/client/disable_alert/<applicant_strid>", methods=['PUT'])
@jwt_required
def disable_alert_to_applicant(applicant_strid):
    """
   This Endpoint disable the alert of an applicant with alert_type coming in the url arguments, Its update the alert data in the applicant collection
    """
    try:
        api_user = get_current_user()
        client_user_id = api_user.get_client_user_id()
        action_log = dict()

        deactivate_alert = request.args.get('disable_alert')
        alert_type = request.args.get('alert_type')
        print("deactivate_alert", deactivate_alert)
        print("alert_type", alert_type)
        if deactivate_alert == 'true':
            action_log['alert_deactivated'] = client_business.deactivate_alert_for_applicant_of_type(client_user_id, applicant_strid, alert_type)

        response = { 'status': True, 'message': "Alert is disabled", 'action_log': action_log }
    except client_business.ClientAPIV3Exception as e:
        response = {"status": False, "message": "ClientAPIV3 error: " + str(e)}
    except Exception as e:
        LOG.exception("Error while disable applicant alert from client", str(e))
        response = {"status":  False, "message": e}
    return make_response_custom_json_encoder(response)


@api.route("/v3/client/disable_issue_found/<applicant_strid>", methods=['PUT'])
@jwt_required
def disable_issue_found_to_applicant(applicant_strid):
    """
    This Endpoint disable the issue found research_results
    """
    try:
        api_user = get_current_user()
        client_user_id = api_user.get_client_user_id()

        deactivate_issue = request.args.get('disable_issue')
        if deactivate_issue == 'true':
            res = client_business.applicant_disable_issue_found(applicant_strid)
            status = True
            message = "Issue Found Box Disabled"
            if isinstance(res, str):
                status = False
                message = res
        else:
            status = False
            message ="deactivate_issue is not true"
        response = { "status": status, "message" : message }
    except InvalidId as e:
        response = {"status": False, "message": "ClientAPIV3 error: " +str(e)}
    except Exception as e:
        LOG.exception("ClientAPIV3 error: ", str(e))
        response = {"status":  False, "message": str(e)}
    return make_response_custom_json_encoder(response)
