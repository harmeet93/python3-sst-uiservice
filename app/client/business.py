from bson import ObjectId
from app import mongo, LOG
from app.exceptions import NoRecordFound, ClientAPIV3Exception, MissingRequestParam
from app.utils import app_business
from app.forms.login import ClientLogin, ApiUserLogin
from app.jwt_auth import issue_access_token_for_api_user_id, jwt_required, issue_access_token_for_api_user, get_routes_ui_aliases_for_api_user
from bson import json_util, ObjectId
from flask import request, make_response, jsonify, Response
import app.notifications_api.business as notification_api_business
import settings
from datetime import datetime


def client_login(request):
    check = app_business.check_protection(request.form)
    if check:
        return check
    form = ClientLogin(request.form)
    if not form.validate():
        if form.is_activated is False:
            pass
        return str(form.errors.values()[0][0])    
    api_user = app_business.find_api_user_by_email(email=form.email._value())
    if api_user:
        access_token = issue_access_token_for_api_user(api_user)
    else:
        LOG.info("Unable to find API user with email {}".format(form.email))
        return ("Combination of email address and password does not exist")
    client_user = app_business.lookup_user_in_collection(user_id=api_user['client_user'], collection_name='client_user')
    user_data = dict()
    user_data['role'] = "user"
    user_data['usertype'] = client_user.get("role", 'Administrator')
    user_data['suspended'] = False
    user_data['email'] = api_user['email']
    try:
        user_data['first_name'] = client_user['first_name']
        user_data['last_name'] = client_user['last_name']
        user_data["can_add_users"] = client_user.get("can_add_user", False)
        user_data["can_add_applicants"] = client_user.get("can_add_applicants", False)

    except KeyError as e:
        LOG.info('Log in client_user record not found {}'.format(e))
        try:
            applicant = app_business.lookup_user_in_collection(user_id=api_user['applicants'], collection_name='applicants')
            user_data['first_name'] = applicant['first_name']
            user_data['last_name'] = applicant['last_name']
            user_data["can_add_users"] = False
            user_data["can_add_applicants"] = False
        except KeyError as e:
            LOG.error('Unable to process user profile {}'.format(e))
            return ("Combination of email address and password does not exist")

    # TODO Deprecate
    permitted_products = []
    perms = api_user.get('permissions', [])
    if "license_search" in perms:
        permitted_products.append('License')

    if "candidate_scoring" in perms:
        permitted_products.append('Cascore')

    if "background_checking" in perms:
        permitted_products.append('Background checking')
    LOG.debug(permitted_products)
    portals = []
    portals = app_business.deduce_portals_from_user_groups(user_groups=api_user.get('user_groups', []))

    user_data.update({
        "access_token": access_token,
        "portals": portals,
        "permitted_products": permitted_products
    })

    return user_data


def lookup_user_in_collection(user_id, collection_name):
    user = mongo[collection_name].find_one({"_id": ObjectId(user_id)})
    return user


def get_clients_data(params):
    try:
        client_user = mongo.client_user.find_one({
                "_id": ObjectId(params['client_user_id'])
            })

        if not client_user:
            raise NoRecordFound("No client found")

        client = mongo.clients.find_one({
            '_id': ObjectId(params['client_id'])
        })

        return {
            "status": True,
            "client_user": client_user,
            "client": client
        }

    except Exception as e:
        return {
            "status": False,
            "message": "Error : %s" % e
        }


def get_notifications(client_id):
    """
    This function is used to fetch notifications and return the 10 notifications
    And For dev purpose its return max 10 same notifications for any user
    """
    notifications = mongo['notifications']
    # res = notifications.find({'client_id': client_id})

    # For dev purposes return max 10 same notifications for any user.
    res = notifications.find().limit(10)

    if res:
        return list(res)
    return None


def client_send_applicant_email(client_user_id, applicant_strid, data):
    """
    This function fetching the details of client and client user and send mail from the client then create dashboard notifiation for applicant in DB
    """
    client_user = app_business.lookup_user_in_collection(client_user_id, 'client_user')
    client = app_business.lookup_user_in_collection(client_user['client_id'], 'clients')

    subject = data.get('subject', '{} from {} requests something done on Vetty'.format( client_user['first_name'], client['client_name']) )

    template_path = 'email_templates/custom_new_letter.html'
    context = dict()
    context["custom_text"] = data['email_content']
    email_response = app_business.send_email_with_template(
        email_to=data['email_to'],
        email_from=data['email_from'],
        subject=subject,
        context=context,
        template_path=template_path
    )

    dashboard_notification = { 'notification_type': 'client_email_sent', 'notification_text': '{} sent email on subject {}'.format(client_user['first_name'], subject), 'priority': 1, 'is_active': True, }
    notification_api_business.create_dashboard_notification_for_applicant(ObjectId(applicant_strid), dashboard_notification)

    return email_response


def deactivate_alert_for_applicant_of_type(client_user_id, applicant_strid, alert_type):
    """
    This function check alert type and disable the dashboard alert
    """
    if alert_type is None:
        raise MissingRequestParam("No alert_type to disable specified")
    return notification_api_business.disable_dashboard_alert_of_type_for_applicant(alert_type, ObjectId(applicant_strid), reason="Action performed by {} at {}".format(client_user_id, datetime.now()))


def activate_clients_user(params):
    """
    This function is activate the client
    """
    client = mongo.client_user.update({
            "_id": ObjectId(params['client_user_id']),
            "client_id": params['client_id']
        }, {
            "$set": {
                "activated": True
            }
        })
    return True


def change_clients_role(params):
    """
    This function is change client roles
    """
    client = mongo.client_user.update({
            "_id": ObjectId(params['client_user_id']),
            "client_id": params['client_id']
        }, {
            "$set": {
                "role": params["data"].get("role")
            }
        })
    return True


def applicant_disable_issue_found(applicant_id):
    """
    This function update the applicant collection and set research_results collection and check some data also 
    """
    applicant = mongo.applicants.find_one({
        "_id": ObjectId(applicant_id)
    })

    if applicant is None:
        return "No applicant found, cannot update"

    if applicant["package"] is None:
        return "No applicant order package is found"

    applicant_update = mongo.applicants.update({
        "_id": ObjectId(applicant_id)
    }, {
        "$set": {
            "research_results.disable_issue_box": True,
        }
    })

    return True
