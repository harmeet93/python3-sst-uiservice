from bson import ObjectId
from app import mongo, LOG
from app.exceptions import NoRecordFound, LoginError
from functools import wraps
from flask import request, jsonify
from werkzeug.security import check_password_hash,generate_password_hash
from app.forms.login import AdminLogin, ApiUserLogin
from app.utils import app_business
from app.jwt_auth import issue_access_token_for_api_user_id, jwt_required, issue_access_token_for_api_user, get_routes_ui_aliases_for_api_user 
from bson import json_util, ObjectId
from flask import request, make_response, jsonify, Response
from app.utils.util import decrypt_timeless_token, generate_random_string, encrypt_token, decrypt_timed_token
import os,re,calendar
from datetime import datetime
import json
import requests
from flask import Flask, render_template
from flask_weasyprint import HTML, render_pdf, CSS
import os



        
def admin_login(request):
    form = ApiUserLogin(request.form)
    if not form.validate():
        api_user_id = backup_admin_login_get_api_user(request)
        if not api_user_id:
            return str(list(form.errors.values())[0][0])
    else:
        api_user_id = form.user._id
    
    admin_user = app_business.find_by_email_user_in_collection(form.email.data, 'admin_user')
    admin_user['api_user_id'] = api_user_id
    return admin_user

def format_client_details(client_record):
    client_industry = client_record.get('client_industry', None)

    if client_industry is not None:
        industry = client_industry.get('industry', None)
    else:
        industry = None

    client_detail = {
        "id": str(client_record["_id"]),
        "client_name": client_record.get('client_name', None),
        "client_industry": industry,
        "contact_email": client_record.get('contact_email', None),
        "contact_first_name": client_record.get('contact_first_name', None),
        "contact_last_name": client_record.get('contact_last_name', None),
        "preferences": client_record.get("preferences", None),
        "created": client_record.get('created', None),
        "activated": client_record.get('activated', False)
    }
    return client_detail


def format_client_user_details(client_user_record):
    clur = client_user_record
    client_user_details = {
        'first_name': clur['first_name'],
        'last_name': clur['last_name'],
        'email': clur['email'],
        'suspended': clur.get('suspended', False),
        'activated': clur.get('activated', False),
        'add_users': clur.get('add_users', False),
        'mobile': clur.get('mobile_number', False),
        'mobile_verified': clur.get('mobile_verified', False),
        'add_applicants': clur.get('add_applicants', False),
        'added_on': clur.get('created')
                            }
    return client_user_details


def get_clients(page, results):
    """gets the list of clients
    """
    offset = (page-1)*results
    lim = results

    test_clients = [
        ObjectId("5bc8b672056f7c8dc32562c3"), ObjectId("5ad06cb2056f7c3499bc9d2e"),
        ObjectId("5ad06cb2056f7c3499bc9d2e"), ObjectId("5a4b3a53056f7c5487781798"),
        ObjectId("5a4b3a53056f7c5487781798"), ObjectId("58884ff19c5d396319ef9c09"),
        ObjectId("58894ff19c5d396319ef9c09")]

    clients_total = mongo.clients.find({
            "is_approved": True,
            '_id': {'$nin': test_clients}
        })
    total = clients_total.count()
    clients = clients_total.skip(offset).limit(lim).sort('created', -1)
    clients_list = []
    
    for client in clients:
        client_compile = format_client_details(client_record=client)

        clients_list.append(client_compile)
    resp = {
        "clients": clients_list,
        "total": total,
        "results": results,
        "page": page
    }
    return resp

def backup_admin_login_get_api_user(request):
    form = AdminLogin(request.form)
    if not form.validate():
        if form.is_activated is False:
            return None
    else:
        admin_user = form.user
        api_user = app_business.find_api_user_by_email(admin_user.email)
        if not api_user:
            user_group = kwargs.get('user_group_to_assign', None)
            if user_group is None:
                return jsonify({"success": False,
                                "message": "No user group found"})
            api_user_id = app_business.create_api_user_from_user_in_collection(
                user_id=admin_user._id, collection_name="admin_user"
            )
            app_business.assign_user_group_to_api_user(user_oid=api_user_id, group_oid=user_group)
            return api_user_id
        else:
            return api_user['_id']

def get_county_fees(suborders):
    county_found = []
    for suborder in suborders:
        if 'inputs' in suborder:
            county = mongo.county_fees.find_one(
                {'state_short': suborder.get('inputs').get('state').upper(), 'county': suborder.get('inputs').get('county').upper()},
                {'county': 1, 'state': 1, 'court_fee': 1, '_id': 0})
        else:
            county = mongo.county_fees.find_one(
                {'state_short': suborder.get('state').upper(), 'county': suborder.get('county').upper()},
                {'county': 1, 'state': 1, 'court_fee': 1, '_id': 0})
        if county is None:
            county = {'county': 'NA', 'state': 'NA', 'court_fee': 0.0}
        county_found.append(county)
    print("country fee")
    print(county_found)    
    return county_found

def get_applicant_additional_cost(applicant, start_date , end_date):
    additional_costs = []
    for additional_cost in applicant.get('package', dict()).get('additional_cost', []):
        if isinstance(additional_cost, dict):
            if start_date <= additional_cost.get('created_at') <= end_date:
                cost = additional_cost.get('additional_cost', 0)
                additional_costs.append(cost)
    print("additional_costs", additional_costs)            
    return sum(additional_costs)


def get_mvr_fees(cases, additional=None, report_results_suborders=None):
    fees = []
    for case in cases:
        if additional:
            case = case.get('drivrec', dict()).get('inputs')
        mvr_fee = mongo.states.find_one(
            {'StateName': case.get('license_state'), 'StateCode': case.get('licenseState_shortName')})
        if report_results_suborders:
            case = case.get('inputs')
            mvr_fee = mongo.states.find_one({'StateCode': case.get('dl_state')})
        if mvr_fee is None:
            mvr_fee = {'mvr_fee': 1.00}
        fees.append(mvr_fee.get('mvr_fee', 0))
    print("fees ", fees)    
    return fees


def additional_check_running_fee(check):
    product = mongo.products.find_one({'code':check})
    price = product.get('price')
    return price
    # return 2

def get_renewals_uploads(client_id, cost_center, cost_centers, client_state, start_date, end_date, data, additional_check_run_fee):
    try:
        if cost_center and cost_center != 'disabled':
            applicants = mongo.applicants.find(
                {'created': {"$lte": start_date}, 'cost_center': cost_center, 'package.renewable': True,
                 'client_id': ObjectId(client_id), 'report_meta.order_id_alias': {"$exists": True}
                 })
        elif cost_center == 'disabled':
            applicants = mongo.applicants.find({'created': {"$lte": start_date},
                                                '$and': [{'cost_center': {"$nin": cost_centers}},
                                                    {'cost_center': {"$exists": True}}], 'package.renewable': True,
                                                'client_id': ObjectId(client_id),
                                                'report_meta.order_id_alias': {"$exists": True}
                                                })
        else:
            applicants = mongo.applicants.find(
                {'created': {"$lte": start_date}, 'cost_center': {"$exists": False}, 'package.renewable': True,
                 'client_id': ObjectId(client_id), 'report_meta.order_id_alias': {"$exists": True}
                 })

        for applicant in applicants:
            try:
                applicant_additional_county = []
                applicant_report_county = []
                applicant_report_county_fee = []
                drivrec_cases_fees = []
                applicant_court_fees = []
                no_additional_check_run = []
                applicant_name = str(applicant.get('first_name', 'NA')) + ' ' + str(applicant.get('last_name', 'NA'))
                order_date = applicant.get('created').strftime("%m/%d/%Y")
                client_user_id = applicant.get('client_user_id', 'NA')
                client_user = "client_user_name(client_user_id)"
                ssn_id = applicant.get('ssn_id')
                if ssn_id:
                    ssn_id = 'XXX-XX-{}'.format(ssn_id.split('-')[2])
                package = applicant['package']
                checks = applicant['package']['renewals']
                additional_check = applicant.get('package', dict()).get('additional_checks')
                package_price = (float(applicant['package']['renewal_price']))/12
                package_name = applicant.get('package').get('title', 'NA') + '(Renewals)'
                check_code = ['crimcounty', 'drivrec']

                renewal_run_dates = mongo.recurring_renewals.find(
                    {'check_code': {'$in': check_code}, 'is_active': False, 'candidate': applicant['_id']})

                applicant_additional_check = get_applicant_additional_check_info(applicant, start_date, end_date,
                                                                                 additional_check_run_fee)
                applicant['package']['additional_checks'] = applicant_additional_check.get('new_additional_check', [])

                # Get Suborders for additional Check and Report Results
                additional_crimcounty_suborders = applicant_additional_check.get('additional_crim_check').get(
                    'suborders')
                additional_drivrec_suborders = applicant_additional_check.get('additional_drivinglicense_check').get(
                    'suborders')

                additional_check_fee = applicant_additional_check.get('applicant_additional_check_run_fee')

                if additional_check:
                    no_additional_check_run.extend(
                        [len(applicant_additional_check.get('additional_crim_check').get('suborders', [])),
                         len(applicant_additional_check.get('additional_natcrim_check').get('suborders', [])),
                         len(applicant_additional_check.get('additional_insuranceveri_check').get('suborders', [])),
                         len(applicant_additional_check.get('additional_ssntrace_check').get('suborders', [])),
                         len(applicant_additional_check.get('additional_drivinglicense_check').get('suborders', []))])

                if additional_crimcounty_suborders:
                    # Get court fees for county in additional check run
                    additional_check_countys = get_county_fees(additional_crimcounty_suborders)
                    for county in additional_check_countys:
                        if county:
                            applicant_additional_county.append(county)
                            court_fees = float(county.get('court_fee'))
                            applicant_court_fees.append(court_fees)

                additional_drivrec_fees = []
                if additional_drivrec_suborders:
                    additional_drivrec_fees = get_mvr_fees(additional_drivrec_suborders, additional=True)
                    # applicant_court_fees.append(sum(additional_drivrec_fees))
                    applicant_court_fees.append(11)

                applicant_additional_county_fee = [fee['court_fee'] for fee in applicant_additional_county]

                for renewal_run_date in renewal_run_dates:
                    if start_date <= renewal_run_date['check_run_date'] <= end_date:
                        if renewal_run_date['check_code'] == 'crimcounty':
                            # Get court fees for county in report result
                            crimcounty_report_results = dict()
                            for key in applicant.get('report_results', dict()).get('crimcounty', dict()):
                                crimcounty_report_results = applicant.get('report_results', dict()).get('crimcounty')[
                                    key]

                            crimcounty_report_results_suborders = crimcounty_report_results.get('suborders')
                            new_crimcounty_report_results_suborders = remove_duplicate_counties_report_result(
                                additional_crimcounty_suborders, crimcounty_report_results_suborders, key)

                            if new_crimcounty_report_results_suborders:
                                report_results_countys = get_county_fees(new_crimcounty_report_results_suborders)
                                for county in report_results_countys:
                                    if county:
                                        applicant_report_county.append(county)
                                        court_fees = float(county.get('court_fee'))
                                        applicant_court_fees.append(court_fees)

                            applicant_report_county_fee = [fee['court_fee'] for fee in applicant_report_county]

                        if renewal_run_date['check_code'] == 'drivrec':
                            # Get state fees for drivinglicense in report result
                            drivrec_report_results = dict()
                            for key in applicant.get('report_results', dict()).get('drivinglicense', dict()):
                                drivrec_report_results = applicant.get('report_results', dict()).get('drivinglicense')[
                                    key]

                            drivrec_cases = drivrec_report_results.get('suborders')
                            new_drivrec_report_results_suborders = remove_duplicate_mvr_record_drivinglicense_cases(
                                additional_drivrec_suborders, drivrec_cases)

                            if new_drivrec_report_results_suborders:
                                drivrec_cases_fees = get_mvr_fees(new_drivrec_report_results_suborders,
                                                                  report_results_suborders=True)
                                applicant_court_fees.append(sum(drivrec_cases_fees))

                            applicant_report_county_fee = [fee['court_fee'] for fee in applicant_report_county]

                # adding tax value ,state fees
                state_fees = sum(applicant_court_fees)

                # calculating tax value
                if client_state:
                    state = mongo.states.find_one({'StateCode': client_state})
                    tax = state.get('StateTax') if state else 0
                else:
                    tax = 0

                total_check_price = float(package_price) + float(additional_check_fee)
                tax_value = total_check_price * float(tax) / 100

                # check if applicant has some discount and calculate total price after deducting discount

                discount = get_applicant_discount(applicant, start_date, end_date)
                additional_cost = get_applicant_additional_cost(applicant, start_date, end_date)

                total_amount = total_check_price + tax_value + state_fees - discount + additional_cost

                applicant = {'order_date': order_date, 'created_by': client_user, 'package': package, 'checks': checks,
                             'package_price': "%.2f" % package_price,
                             'additional_check_fee': "%.2f" % additional_check_fee,
                             'report_county': applicant_report_county,
                             'report_county_fee': "%.2f" % sum(applicant_report_county_fee),
                             'additional_county_fee': "%.2f" % sum(applicant_additional_county_fee),
                             'total_county_fee': "%.2f" % (
                                     sum(applicant_additional_county_fee) + sum(applicant_report_county_fee) + sum(
                                 additional_drivrec_fees) + sum(drivrec_cases_fees)),
                             'pkg_check_price': "%.2f" % total_check_price, 'ssn_id': str(ssn_id), 'tax': float(tax),
                             'state_fees': "%.2f" % state_fees, 'discount': "%.2f" % discount,
                             'tax_value': "%.2f" % tax_value, 'name': applicant_name.title(),
                             'package_name': package_name, 'additional_county': applicant_additional_county,
                             'additional_cost': "%.2f" % additional_cost,
                             'drivrec_cases_fees': "%.2f" % sum(drivrec_cases_fees),
                             'additional_drivrec_fees': additional_drivrec_fees,
                             'sum_additional_drivrec_fees': "%.2f" % sum(additional_drivrec_fees),
                             'final_amount': "%.2f" % total_amount
                             }
                data['all_state_fees'].append(float(state_fees))
                data['all_tax'].append(float(tax_value))
                data['no_additional_check_run'].extend(no_additional_check_run)
                data['all_pkgcheck_price'].append(float(total_check_price))
                data['all_discount'].append(float(discount))
                data['all_additional_cost'].append(float(additional_cost))
                data['all_pkg_price'].append(float(package_price))
                data['all_amount'].append(float(total_amount))
                data['all_applicant'].append(applicant)

            except Exception as e:
                LOG.info(e)
                data['applicant_skipped'].append("{} error in applicant  {}  ".format(e, applicant['_id']))
                continue

        on_demand_renewal_applicant = get_on_demand_renewals_applicant(client_id, cost_center, cost_centers, client_state, start_date, end_date,data, additional_check_run_fee)

    except Exception as e:
        raise Exception('%s' %e)

    return on_demand_renewal_applicant

def get_on_demand_renewals_applicant(client_id, cost_center, cost_centers, client_state, start_date, end_date, data, additional_check_run_fee):
    try:
        if cost_center and cost_center != 'disabled':
            applicants = mongo.applicants.find({'created': {"$lte": start_date}, 'cost_center': cost_center,
                                                'package.additional_individual_check': {'$exists': True},
                                                'report_meta.order_id_alias': {"$exists": True},
                                                'client_id': ObjectId(client_id)
                                                })
        elif cost_center == 'disabled':
            applicants = mongo.applicants.find({'created': {"$lte": start_date},
                                                '$and': [{'cost_center': {"$nin": cost_centers}},
                                                    {'cost_center': {"$exists": True}}],
                                                'package.additional_individual_check': {'$exists': True},
                                                'report_meta.order_id_alias': {"$exists": True},
                                                'client_id': ObjectId(client_id)
                                                })
        else:
            applicants = mongo.applicants.find({'created': {"$lte": start_date}, 'cost_center': {"$exists": False},
                                                'package.additional_individual_check': {'$exists': True},
                                                'report_meta.order_id_alias': {"$exists": True},
                                                'client_id': ObjectId(client_id)
                                                })

        applicants_found = get_applicant_with_on_demand_renewals_previous_month(applicants, start_date, end_date)
        for applicant in applicants_found:
            try:
                applicant_additional_county = []
                applicant_report_county = []
                drivrec_cases_fees = []
                applicant_court_fees = []
                no_additional_check_run = []
                applicant_name = str(applicant.get('first_name', 'NA')) + ' ' + str(
                    applicant.get('last_name', 'NA'))
                order_date = applicant.get('created').strftime("%m/%d/%Y")
                client_user_id = applicant.get('client_user_id', 'NA')
                client_user = "client_user_name(client_user_id)"
                ssn_id = applicant.get('ssn_id')
                if ssn_id:
                    ssn_id = 'XXX-XX-{}'.format(ssn_id.split('-')[2])
                package = applicant['package']
                checks = get_billing_month_on_demand_renewal_checks(applicant, start_date, end_date)
                additional_check = applicant.get('package', dict()).get('additional_checks')
                package_price = 0.00
                package_name = applicant.get('package').get('title', 'NA') + '(New Checks)'

                applicant_additional_check = get_applicant_additional_check_info(applicant, start_date, end_date,
                                                                                 additional_check_run_fee)
                applicant['package']['additional_checks'] = applicant_additional_check.get('new_additional_check',
                                                                                           [])

                # Get Suborders for additional Check and Report Results
                additional_drivrec_suborders = applicant_additional_check.get(
                    'additional_drivinglicense_check').get('suborders')

                additional_check_fee = applicant_additional_check.get('applicant_additional_check_run_fee')

                if additional_check:
                    no_additional_check_run.extend(
                        [len(applicant_additional_check.get('additional_crim_check').get('suborders', [])),
                         len(applicant_additional_check.get('additional_insuranceveri_check').get('suborders', [])),
                         len(applicant_additional_check.get('additional_natcrim_check').get('suborders', [])),
                         len(applicant_additional_check.get('additional_ssntrace_check').get('suborders', [])), len(
                            applicant_additional_check.get('additional_drivinglicense_check').get('suborders',
                                                                                                  []))])

                additional_drivrec_fees = []
                if additional_drivrec_suborders:
                    additional_drivrec_fees = get_mvr_fees(additional_drivrec_suborders, additional=True)
                    applicant_court_fees.append(sum(additional_drivrec_fees))

                applicant_additional_county_fee = [fee['court_fee'] for fee in applicant_additional_county]
                if 'drivrec' in [check['code'] for check in checks]:
                    # Get state fees for drivinglicense in report result
                    drivrec_report_results = dict()
                    for key in applicant.get('report_results', dict()).get('drivinglicense', dict()):
                        drivrec_report_results = \
                        applicant.get('report_results', dict()).get('drivinglicense')[key]

                    drivrec_cases = drivrec_report_results.get('suborders')
                    new_drivrec_report_results_suborders = remove_duplicate_mvr_record_drivinglicense_cases(
                        additional_drivrec_suborders, drivrec_cases)

                    if new_drivrec_report_results_suborders:
                        drivrec_cases_fees = get_mvr_fees(new_drivrec_report_results_suborders,
                                                          report_results_suborders=True)
                        applicant_court_fees.append(sum(drivrec_cases_fees))

                applicant_report_county_fee = [fee['court_fee'] for fee in applicant_report_county]

                # adding tax value ,state fees
                state_fees = sum(applicant_court_fees)

                # calculating tax value
                if client_state:
                    state = mongo.states.find_one({'StateCode': client_state})
                    tax = state.get('StateTax') if state else 0
                else:
                    tax = 0

                total_check_price = float(package_price) + float(additional_check_fee)
                tax_value = total_check_price * float(tax) / 100

                # check if applicant has some discount and calculate total price after deducting discount

                discount = get_applicant_discount(applicant, start_date, end_date)
                additional_cost = get_applicant_additional_cost(applicant, start_date, end_date)

                total_amount = total_check_price + tax_value + state_fees - discount + additional_cost

                applicant = {'order_date': order_date, 'created_by': client_user, 'package': package,
                             'checks': checks, 'package_price': "%.2f" % package_price,
                             'additional_check_fee': "%.2f" % additional_check_fee,
                             'report_county': applicant_report_county,
                             'report_county_fee': "%.2f" % sum(applicant_report_county_fee),
                             'additional_county_fee': "%.2f" % sum(applicant_additional_county_fee),
                             'total_county_fee': "%.2f" % (
                                     sum(applicant_additional_county_fee) + sum(applicant_report_county_fee) + sum(
                                 additional_drivrec_fees) + sum(drivrec_cases_fees)),
                             'pkg_check_price': "%.2f" % total_check_price, 'ssn_id': str(ssn_id),
                             'tax': float(tax), 'state_fees': "%.2f" % state_fees, 'discount': "%.2f" % discount,
                             'tax_value': "%.2f" % tax_value, 'name': applicant_name.title(),
                             'package_name': package_name, 'additional_county': applicant_additional_county,
                             'additional_cost': "%.2f" % additional_cost,
                             'drivrec_cases_fees': "%.2f" % sum(drivrec_cases_fees),
                             'additional_drivrec_fees': additional_drivrec_fees,
                             'sum_additional_drivrec_fees': "%.2f" % sum(additional_drivrec_fees),
                             'final_amount': "%.2f" % total_amount
                             }
                data['all_state_fees'].append(float(state_fees))
                data['all_tax'].append(float(tax_value))
                data['no_additional_check_run'].extend(no_additional_check_run)
                data['all_pkgcheck_price'].append(float(total_check_price))
                data['all_discount'].append(float(discount))
                data['all_additional_cost'].append(float(additional_cost))
                data['all_pkg_price'].append(float(package_price))
                data['all_amount'].append(float(total_amount))
                data['all_applicant'].append(applicant)

            except Exception as e:
                LOG.info(e)
                data['applicant_skipped'].append("{} error in applicant  {}  ".format(e, applicant['_id']))
                continue

    except Exception as e:
        raise Exception('%s' % e)
    return data


def get_billing_month_on_demand_renewal_checks(applicant,start_date,end_date):
    try:
        new_checks = applicant.get('package').get('additional_individual_check', [])
        checks = []
        for check in new_checks:
            if start_date <= check['created_at'] <= end_date:
                checks.append(check)
        return checks
    except Exception as e:
        raise Exception('%s' % e)
def get_applicant_with_on_demand_renewals_previous_month(applicants, start_date, end_date):
    try:
        applicants_found = []
        for applicant in applicants:
            new_checks = applicant.get('package').get('additional_individual_check', [])
            check_ran = False
            for check in new_checks:
                if start_date <= check['created_at'] <= end_date:
                    check_ran = True
            if check_ran:
                applicants_found.append(applicant)
        LOG.debug(len(applicants_found))
        return applicants_found

    except Exception as e:
        raise Exception('%s' % e)


def get_packages_client(client_id):
    try:
        packages = mongo.package.find({'client_id': ObjectId(client_id)})
        list_packages = list()
        for p in packages:
            sub_pack = {}
            title = p.get('title','')
            package_id = str(p.get('_id'))
            sub_pack.update(title= title, package_id= package_id)
            list_packages.append(sub_pack)
        return {'status': True, 'packages': list_packages}
    except Exception as e:
        return {'status': False, 'message': e}


def get_invoice_upload_id(params, cost_center, cost_centers, client_name, invoice_id, client_address, admin_user_id):
    try:
        applicant_skipped = []
        no_additional_check_run = []
        all_pkgcheck_price = []
        all_pkg_price = []
        all_amount = []
        all_state_fees = []
        all_discount = []
        all_additional_cost = []
        all_tax = []
        all_applicant = []
        total_additional_checks_price = []
        invoice_date = params.get('invoice_month')
        date = invoice_date.split('-')

        print("invoice_date 1", int(date[0]), int(date[1]))
        day = calendar.monthrange(int(date[0]), int(date[1]))[1]
        print("invoice_date 2", day)
        # start_date = "2018-10-01"
        start_date = datetime(int(date[0]), int(date[1]), 1)
        print("invoice_date 3 ")
        end_date = datetime(int(date[0]), int(date[1]), day).replace(hour=23, minute=59, second=59, microsecond=999999)
        # end_date = "2018-10-30"
        print("invoice_date 4")

        print("ooo")

        ssntrace_running_fee = additional_check_running_fee('ssntrace')
        drivinglicense_running_fee = additional_check_running_fee('drivrec')
        natcrim_running_fee = additional_check_running_fee('natcrim')
        crimcounty_running_fee = additional_check_running_fee('crimcounty')
        insuranceveri_running_fee = additional_check_running_fee('Insuranceveri')

        additional_check_run_fee = {'ssntrace_running_fee': ssntrace_running_fee,
                                'drivinglicense_running_fee': drivinglicense_running_fee,
                                'natcrim_running_fee': natcrim_running_fee,
                                'crimcounty_running_fee': crimcounty_running_fee,
                                'insuranceveri_running_fee': insuranceveri_running_fee
                                }
        client_physical_address = '{} {} {} {} {}'.format(client_address.get('apt', ''),
                                                          client_address.get('street', ''),
                                                          client_address.get('city', ''),
                                                          client_address.get('state_name', ''),
                                                          client_address.get('pincode', ''))
        
        print("010101010101010101010")
        client_state = client_address.get('state_name')
        client_id = params.get('client_id')

        print("1010101010101010101010")
        # if cost_center and cost_center != 'disabled':
        #     applicants = mongo.applicants.find(
        #         {'created': {"$lte": end_date, "$gte": start_date}, 'cost_center': cost_center,
        #          'client_id': ObjectId(client_id), 'report_meta.order_id_alias': {"$exists": True}
        #          })
        #     print("applicant looking", applicants)
        # elif cost_center == 'disabled':
        #     applicants = mongo.applicants.find(
        #         {'created': {"$lte": end_date, "$gte": start_date}, '$and': [
        #         {'cost_center': {"$nin": cost_centers}}, {'cost_center': {"$exists": True}}], 'client_id': ObjectId(client_id),
        #          'report_meta.order_id_alias': {"$exists": True}
        #          })
        # else:
        applicants = mongo.applicants.find(
            {'created': {"$lte": end_date, "$gte": start_date}, 'client_id': ObjectId(client_id),
             'report_meta.order_id_alias': {"$exists": True}
             })

        print("applicants")
        print(applicants.count())    
        for applicant in applicants:
            # try:
            applicant_report_county = []
            applicant_additional_county = []
            applicant_court_fees = []
            applicant_name = str(applicant.get('first_name'))+' '+str(applicant.get('last_name','NA'))
            order_date = applicant.get('created').strftime("%m/%d/%Y")
            order__date_format = applicant.get('created').strftime("%Y-%m")
            client_user_id = applicant.get('client_user_id','NA')
            # client_user = client_user_name(client_user_id)
            client_user = "client_user_name(client_user_id)"
            ssn_id = applicant.get('ssn_id')
            if ssn_id:
                ssn_id = 'XXX-XX-{}'.format(ssn_id.split('-')[2])
            # applicant_state = applicant.get('address', dict()).get('state')
            package = applicant['package']
            checks = applicant['package']['checklist']
            package_price = applicant['package']['price']
            package_name = applicant.get('package').get('title','NA')
            print("call get_applicant_additional_check_info", additional_check_run_fee)
            # get number of additional crimcounty/nationalcrim/ssntrace/MVR check Run and calculate fees
            applicant_additional_check = get_applicant_additional_check_info(applicant, start_date, end_date,
                                                                             additional_check_run_fee)
            print("again get_applicant_additional_check_info", applicant_additional_check)
            applicant['package']['additional_checks'] = applicant_additional_check.get('new_additional_check', [])

            # Get additional check and report_results suborders of applicant
            additional_crimcounty_suborders = applicant_additional_check.get('additional_crim_check').get(
                'suborders')
            additional_drivrec_suborders = applicant_additional_check.get('additional_drivinglicense_check').get(
                'suborders')

            drivrec_report_results = dict()
            for key in applicant.get('report_results', dict()).get('drivinglicense', dict()):
                drivrec_report_results = applicant.get('report_results', dict()).get('drivinglicense')[key]

            drivrec_cases = drivrec_report_results.get('suborders')

            crimcounty_report_results = dict()
            for key in applicant.get('report_results', dict()).get('crimcounty', dict()):
                crimcounty_report_results = applicant.get('report_results', dict()).get('crimcounty')[key]

            crimcounty_report_results_suborders = crimcounty_report_results.get('suborders')

            additional_check_fee = applicant_additional_check.get('applicant_additional_check_run_fee')

            # Exclude county from report_results which are in additional_check
            if applicant.get('report_results', dict()).get('crimcounty'):
                new_crimcounty_report_results_suborders = remove_duplicate_counties_report_result(
                    additional_crimcounty_suborders, crimcounty_report_results_suborders, key)

                if new_crimcounty_report_results_suborders:
                    report_results_countys = get_county_fees(new_crimcounty_report_results_suborders)
                    for county in report_results_countys:
                        if county:
                            applicant_report_county.append(county)
                            court_fees = float(county.get('court_fee'))
                            applicant_court_fees.append(court_fees)

            # Calculate Fees For Additional Check And Report Result Suborders
            if additional_crimcounty_suborders:
                additional_check_countys = get_county_fees(additional_crimcounty_suborders)
                for county in additional_check_countys:
                    if county:
                        applicant_additional_county.append(county)
                        court_fees = float(county.get('court_fee'))
                        applicant_court_fees.append(court_fees)

            # Exclude additional mvr cases from drivingLicenseInfo cases
            new_drivrec_cases = remove_duplicate_mvr_record_drivinglicense_cases(additional_drivrec_suborders,drivrec_cases)

            additional_drivrec_fees = []
            if additional_drivrec_suborders:
                additional_drivrec_fees = get_mvr_fees(additional_drivrec_suborders, additional=True)
                applicant_court_fees.append(sum(additional_drivrec_fees))

            drivrec_cases_fees = []
            if new_drivrec_cases:
                drivrec_cases_fees = get_mvr_fees(new_drivrec_cases, report_results_suborders=True)
                applicant_court_fees.append(sum(drivrec_cases_fees))

            applicant_report_county_fee = [fee['court_fee']for fee in applicant_report_county]
            applicant_additional_county_fee = [fee['court_fee']for fee in applicant_additional_county]

            # adding tax value ,state fees
            state_fees = sum(applicant_court_fees)

            # calculating tax value
            if client_state:
                state = mongo.states.find_one({'StateCode': client_state})
                tax = state.get('StateTax') if state else 0
            else:
                tax = 0

            total_check_price = float(package_price) + float(additional_check_fee)
            tax_value = total_check_price * float(tax)/100

            discount = get_applicant_discount(applicant, start_date, end_date)
            additional_cost = get_applicant_additional_cost(applicant, start_date, end_date)

            total_amount = total_check_price + tax_value + state_fees - discount + additional_cost

            # Appending all value to get final values
            if 'additional_checks' in package:
                no_additional_check_run.extend(
                    [len(applicant_additional_check.get('additional_crim_check').get('suborders', [])),
                     len(applicant_additional_check.get('additional_natcrim_check').get('suborders', [])),
                     len(applicant_additional_check.get('additional_insuranceveri_check').get('suborders', [])),
                     len(applicant_additional_check.get('additional_ssntrace_check').get('suborders', [])),
                     len(applicant_additional_check.get('additional_drivinglicense_check').get('suborders', []))]
                )

            total_additional_checks_price.append(float(additional_check_fee))
            all_state_fees.append(float(state_fees))
            all_tax.append(tax_value)
            all_pkgcheck_price.append(total_check_price)
            all_discount.append(discount)
            all_additional_cost.append(additional_cost)
            all_pkg_price.append(float(package_price))
            all_amount.append(float(total_amount))

            applicant = {'order_date': order_date,
                         'order__date_format' : order__date_format,
                         'created_by': client_user, 'package': package, 'checks': checks,
                         'additional_drivrec_fees': additional_drivrec_fees,
                         'sum_additional_drivrec_fees': "%.2f" % sum(additional_drivrec_fees),
                         'drivrec_cases_fees': "%.2f" % sum(drivrec_cases_fees),
                         'package_price': "%.2f" % float(package_price),
                         'additional_check_fee': "%.2f" % additional_check_fee,
                         'additional_cost': "%.2f" % additional_cost, 'report_county': applicant_report_county,
                         'additional_county': applicant_additional_county,
                         'report_county_fee': "%.2f" % sum(applicant_report_county_fee),
                         'additional_county_fee': "%.2f" % sum(applicant_additional_county_fee),
                         'total_county_fee': "%.2f" % (
                                 sum(applicant_additional_county_fee) + sum(applicant_report_county_fee) + sum(
                             additional_drivrec_fees) + sum(drivrec_cases_fees)),
                         'pkg_check_price': "%.2f" % total_check_price, 'ssn_id': str(ssn_id), 'tax': float(tax),
                         'state_fees': "%.2f" % state_fees, 'discount': "%.2f" % discount,
                         'tax_value': "%.2f" % tax_value, 'name': applicant_name.title(),
                         'package_name': package_name, 'final_amount': "%.2f" % total_amount
                         }
            all_applicant.append(applicant)

            # except Exception as e:
            #     applicant_skipped.append("{} error in candidate {} with id {} <br> <br> ".format(e,applicant_name.title(),applicant['_id']))
            #     continue


        print("for end")    
        data = {'all_applicant': all_applicant, 'applicant_skipped': applicant_skipped,
                'no_additional_check_run': no_additional_check_run,
                'all_pkgcheck_price': all_pkgcheck_price, 'all_pkg_price': all_pkg_price, 'all_amount': all_amount,
                'all_state_fees': all_state_fees, 'all_discount': all_discount,
                'all_additional_cost': all_additional_cost, 'all_tax': all_tax
                }

        renewal_applicant = get_renewals_uploads(client_id, cost_center, cost_centers,client_state, start_date, end_date, data,
                                                 additional_check_run_fee)

        all_applicant = renewal_applicant['all_applicant']
        no_additional_check_run = renewal_applicant['no_additional_check_run']
        all_state_fees = renewal_applicant['all_state_fees']
        all_tax = renewal_applicant['all_tax']
        all_pkgcheck_price = renewal_applicant['all_pkgcheck_price']
        all_discount = renewal_applicant['all_discount']
        all_additional_cost = renewal_applicant['all_additional_cost']
        all_pkg_price = renewal_applicant['all_pkg_price']
        all_amount = renewal_applicant['all_amount']
        applicant_skipped = renewal_applicant['applicant_skipped']

        data = {'all_applicant': all_applicant, 'applicant_skipped': applicant_skipped,
                'no_additional_check_run': no_additional_check_run,
                'all_pkgcheck_price': all_pkgcheck_price, 'all_pkg_price': all_pkg_price, 'all_amount': all_amount,
                'all_state_fees': all_state_fees, 'all_discount': all_discount,
                'all_additional_cost': all_additional_cost, 'all_tax': all_tax
                }

        additional_check_applicant = get_additional_checks_applicants(client_id, cost_center, cost_centers,client_state, start_date, end_date, data ,
                                                                      additional_check_run_fee)

        all_applicant = additional_check_applicant['all_applicant']
        all_state_fees = additional_check_applicant['all_state_fees']
        all_tax = additional_check_applicant['all_tax']
        no_additional_check_run = additional_check_applicant['no_additional_check_run']
        all_pkgcheck_price = additional_check_applicant['all_pkgcheck_price']
        all_discount = additional_check_applicant['all_discount']
        all_additional_cost = additional_check_applicant['all_additional_cost']
        all_pkg_price = additional_check_applicant['all_pkg_price']
        all_amount = additional_check_applicant['all_amount']
        applicant_skipped = additional_check_applicant['applicant_skipped']

        if len(all_applicant) <= 0:
            return None, None

        check_run_fee = {
            'ssntrace_running_fee': "%.2f" % ssntrace_running_fee,
            'drivinglicense_running_fee': "%.2f" % drivinglicense_running_fee,
            'natcrim_running_fee': "%.2f" % natcrim_running_fee,
            'crimcounty_running_fee': "%.2f" % crimcounty_running_fee,
            'insuranceveri_running_fee': "%.2f" % insuranceveri_running_fee
        }

        response = {
            'status': True,
            'applicants': all_applicant,
            'total_applicant': len(all_applicant),
            'total_pkgcheck_price': "%.2f" % sum(all_pkgcheck_price),
            'total_additional_checks_price': "%.2f" % sum(total_additional_checks_price),
            'total_pkg_price': "%.2f" % sum(all_pkg_price),
            'total_discount': "%.2f" % sum(all_discount),
            'total_additional_cost': "%.2f" % sum(all_additional_cost),
            'client_physical_address': client_physical_address,
            'total_tax': "%.2f" % sum(all_tax),
            'invoice_date': '{} - {}'.format(start_date.strftime("%m/%d/%Y"), end_date.strftime("%m/%d/%Y")),
            'client_name': str(client_name),
            "state_fees": "%.2f" % sum(all_state_fees),
            'invoice_id': invoice_id,
            "no_additional_check_run": sum(no_additional_check_run),
            'total_amount': "%.2f" % sum(all_amount)
        }
        context = {
            "response": response,
            "check_run_fee": check_run_fee
        }

        # print("hey there muz")
        # data_to_insert = mongo.candidate_invoice.insert(
        #     {"response": context.get("response"), "check_run_fee": context.get("check_run_fee"),
        #      "client_id": params.get("client_id")})
        email = mongo.clients.find_one({"_id": ObjectId(params.get("client_id"))})
        # email_id = email.get("billing_email", '')
        email_id = "hussain@aconnexion.com"
        total_amount = context.get("response").get("total_amount")
        invoice_date = context.get("response").get("invoice_date")
        response_email = ''
        _stripe = dict()
        # if email_id:
        #     response_email = stripe_api_customers(email_id, total_amount, invoice_date)
        #     if not response_email.get("invoice_id", ''):
        #         subject = "No client found in stripe with email id"
        #         body = """
        #                 Hi Admin,
        #                 No client with {} email id exists in our stripe customer list.
        #                 """
        #         body = body.format(email_id)
        #         _set_up_client_email_invoice(params.get("client_id"), email_to="support@vetty.co",
        #                                      email_from="support@vetty.co", subject=subject, body=body)
        #     else:
        #         _stripe.update(stripe_details=response_email)
        #         mongo.stripe_invoice.insert(
        #             {"report_level": params.get("report_level"), "invoice_month": params.get("invoice_month"),
        #              "invoice_id": response_email.get("invoice_id"), "client_id": params.get("client_id"),
        #              "created": datetime.now()})
        # else:
        #     response_email = False
        response_email = "hussain@aconnexion.com"
        li = []
        # upload_id, file_name = excel_invoice(params, context, cost_center, cost_centers, client_name, invoice_id,
        #                                      client_address, admin_user_id, applicant_skipped, client_id, date)
        # excel = {"upload_id": upload_id, "file_name": file_name}
        # li.append(excel)

        LOG.debug(applicant_skipped)
        dirname = os.path.dirname(__file__)

        ENV = os.environ.get("DEPLOYMENT_ENV", "local")
        if len(applicant_skipped) > 0:
            send_message(email_to='hardbitec0c0c0@gmail.com', email_from='hardbitec0c0c0@gmail.com',
                         subject='ERROR in applicant While Invoice Generation: {}'.format(ENV),
                         email_content=str(applicant_skipped) + " <br> <br>", from_name=None,
                         attachments=None)

        # template_file_path = settings.TEMPLATES_PATH + "email_templates/invoices/{}_invoice.html".format(
        template_file_path = "/email_templates/invoices/{}_invoice.html".format(
            params.get('report_level'))

        print("---")
        print("---")
        # print(context)
        print(template_file_path)
        print("---")

        html = render_template(template_file_path, response = context)

        # html = render_template('hello.html', response=response)

        pdf = HTML(string=html).write_pdf(stylesheets=[CSS(string='img{ width:250px; height:70px; margin-top: 40px; } body{ font-size: 10px; } table{ width: 100%; }')])
        file_name = '{}-{}-{}.pdf'.format(client_name[0:5], date[1], date[0])

        if os.path.exists(dirname):
            f = open(os.path.join(dirname+ "/pdf_folder/", file_name), 'wb')
            f.write(pdf)

        # li.append("mypdf.pdf")    
        print("file converted pdf success")    
        # WKHTMLTOPDF_PATH = '/usr/local/bin/wkhtmltopdf'
        # upload_path = settings.STATIC_PATH + 'client_docs/{}/invoices/{}/'.format(str(client_id),
        #                                                                           params.get('report_level'))

        # file_name = '{}-{}-{}.pdf'.format(client_name[0:5], date[1], date[0])
        # if cost_center:
        #     file_name = '{}-{}-{}-{}.pdf'.format(client_name[0:5], date[1], date[0], cost_center.title())
        # upload_path = upload_path + file_name
        # options = {
        #     'page-size': 'A4',
        #     'page-width': '98mm',
        #     'encoding': "UTF-8",
        #     'disable-smart-shrinking': None,
        #     'zoom': 0.4
        # }

        # config = pdfkit.configuration(wkhtmltopdf=WKHTMLTOPDF_PATH)
        # upload_file = tempfile.NamedTemporaryFile(suffix=".pdf", mode="r+")
        # pdf = pdfkit.from_string(html, False, configuration=config, options=options)
        # upload_file.write(pdf)
        # upload_file.flush()
        # upload_file.seek(0)
        # _save_new_uploaded_file(upload_file, upload_path)
        # upload_info = dict()
        # upload_info['upload_path'] = upload_path
        # upload_info['file_name'] = file_name
        # upload_id = _register_file_upload(ObjectId(admin_user_id), 'client', upload_path, upload_info)
        # pdf = {"upload_id": upload_id, "file_name": file_name}
        # li.append(pdf)
        return {"status": True, "message": "PDF generated successfully"}

    except Exception as e:
        raise Exception(e)


def get_applicant_additional_check_info(applicant, start_date, end_date, additional_check_run_fee):

    # Get Applicant Additional Check added in Invoice Generation Month
    additional_check = applicant.get('package').get('additional_checks', dict())
    new_additional_check = dict()
    for check, value in additional_check.items():
        suborders = value.get('suborders')
        if suborders:
            suborder_dict = {check: {'suborders': []}}
            for suborder in suborders:
                if check == 'drivrec':
                    created_at = suborder.get(check).get('created_at')
                else:
                    created_at = suborder.get('created_at')
                if created_at:
                    if start_date <= created_at <= end_date:
                        suborder_dict.get(check).get('suborders').append(suborder)
            if suborder_dict.get(check, dict()).get('suborders', []):
                new_additional_check.update(suborder_dict)

    applicant['package']['additional_checks'] = new_additional_check

    additional_crim_check = applicant.get('package', dict()).get('additional_checks', dict()).get('crimcounty', dict())
    additional_insuranceveri_check = applicant.get('package', dict()).get('additional_checks', dict()).get('Insuranceveri', dict())
    additional_natcrim_check = applicant.get('package', dict()).get('additional_checks', dict()).get('natcrim', dict())
    additional_ssntrace_check = applicant.get('package', dict()).get('additional_checks', dict()).get('ssntrace',
                                                                                                      dict())
    additional_drivinglicense_check = applicant.get('package', dict()).get('additional_checks', dict()).get('drivrec',
                                                                                                            dict())

    applicant_additional_check_run_fee = (
                len(additional_crim_check.get('suborders', [])) * additional_check_run_fee.get(
            'crimcounty_running_fee') + len(
            additional_natcrim_check.get('suborders', [])) * additional_check_run_fee.get('natcrim_running_fee') + len(
            additional_ssntrace_check.get('suborders', [])) * additional_check_run_fee.get(
            'ssntrace_running_fee') + len(
            additional_drivinglicense_check.get('suborders', [])) * additional_check_run_fee.get(
            'drivinglicense_running_fee') + len(
            additional_insuranceveri_check.get('suborders', [])) * additional_check_run_fee.get(
            'insuranceveri_running_fee'))

    data = {'additional_crim_check': additional_crim_check, 'additional_natcrim_check': additional_natcrim_check,
            'additional_ssntrace_check': additional_ssntrace_check,
            'additional_insuranceveri_check': additional_insuranceveri_check,
            'additional_drivinglicense_check': additional_drivinglicense_check,
            'applicant_additional_check_run_fee': applicant_additional_check_run_fee,
            'new_additional_check': new_additional_check
            }
    return data


def get_applicant_discount(applicant, start_date, end_date):
    discount_costs = []
    for discount_cost in applicant.get('package', dict()).get('discount_cost', dict()):
        if start_date <= discount_cost.get('created_at') <= end_date:
            cost = discount_cost.get('discount', 0)
            discount_costs.append(cost)
    return sum(discount_costs)


def generate_invoice(params, admin_user_id):
    # try:
    client = app_business.lookup_user_in_collection(params.get('client_id'), 'clients')
    if not client:
        raise Exception("Client not found")
    print("there")    
    q = {}
    q['client_id'] = ObjectId(params.get('client_id'))
    q['invoice_month'] = params.get('invoice_month')
    q['report_level'] = params.get('report_level')
    q['is_deleted'] = False
    print("oeoeoeoeo")
    print(q)
    found = mongo["invoice"].find(q)

    if found.count() == 0:
        print("found invoice here")
        client_name = client.get('client_name')
        client_cost_centers = client.get('cost_centers', [])
        cost_centers = [cost_center.get('cost_center') for cost_center in client_cost_centers]
        cost_centers.extend([None, 'disabled'])
        client_address = client.get('physical_address')
        all_upload = []
        print("cost cost_center ", cost_centers)
        for cost_center in cost_centers:
            print("=======1")
            invoice_id = generate_random_string(6).upper()
            print("------1")
            return get_invoice_upload_id(params, cost_center, cost_centers, client_name, invoice_id,
                                            client_address, admin_user_id)
            print(uploads)
            print("=======2")

            if uploads and None not in uploads:
                print("--------====11")
                for upload in uploads:
                    file_name = upload["file_name"]
                    upload_id = upload["upload_id"]
                    all_upload.append(upload_id)
                    client_invoice = {"invoice_month": params.get('invoice_month'),
                                      "report_level": params.get('report_level', "summary"),
                                      "client_id": ObjectId(params.get('client_id')), "client_name": client_name,
                                      "billing_email": client.get('billing_email', client.get('contact_email')),
                                      "invoice_id": invoice_id, "file_name": file_name, "created": datetime.now(),
                                      "updated": datetime.now(), "upload_id": upload_id, "is_deleted": False
                                      }
                    stripe_details = upload.get('stripe_details','')
                    if stripe_details:
                        client_invoice.update(stripe_invoice=stripe_details)
                    if cost_center:
                        client_invoice['cost_center'] = cost_center
                    insert_result = mongo.invoice.insert(client_invoice)
                    if '.pdf' in file_name:
                        client_id = params.get('client_id')
                        client_email = client.get('billing_email','owner_email')
                        month_no = params.get('invoice_month').split('-')[1]
                        year = params.get('invoice_month').split('-')[0]
                        date_with_month = dt.date(int(year),int(month_no),1).strftime("%B")
                        date_in_mail = "{0}, {1}".format(date_with_month,year)
                        subject = "Vetty invoice for the month of {}".format( date_in_mail)
                        body = """
                        Hi,
                        Please find attached invoice for the month of {0}
                        """
                        body = body.format(date_in_mail)
                        email_response = _set_up_client_email_invoice(client_id=client_id, email_to=client_email, email_from="admin@vetty.co", subject=subject, body=body, upload_id=upload_id)

        if all_upload and None not in all_upload:
            return {'status': True, 'message': 'Client Invoice Generated for the month'}
        else:
            return {'status': False, 'message': 'No applicant found for the specified month'}
    else:
        return {'status': False, 'message': 'Invoice already generated for this month for this client'}

    # except Exception as e:
    #     return {'status': False, 'message': e}



def get_additional_checks_applicants(client_id, cost_center, cost_centers, client_state, start_date, end_date, data, additional_check_run_fee):
    # try:
    if cost_center and cost_center != 'disabled':
        applicants = mongo.applicants.find(
            {'created': {"$lt": start_date}, 'cost_center': cost_center, 'client_id': ObjectId(client_id),
             'report_meta.order_id_alias': {"$exists": True}, 'package.renewable': {'$ne': True}
             })
    elif cost_center == 'disabled':
        applicants = mongo.applicants.find({'created': {"$lt": start_date}, 'client_id': ObjectId(client_id),
                                            'report_meta.order_id_alias': {"$exists": True},
                                            '$and': [{'cost_center': {"$nin": cost_centers}},
                                                {'cost_center': {"$exists": True}}],
                                            'package.renewable': {'$ne': True}
                                            })
    else:
        applicants = mongo.applicants.find({'created': {"$lt": start_date}, 'client_id': ObjectId(client_id),
                                            'report_meta.order_id_alias': {"$exists": True},
                                            'cost_center': {"$exists": False},
                                            'package.renewable': {'$ne': True}
                                            })

    list_applicants = get_missed_additional_check_applicant(applicants, start_date, end_date)
    for list_applicant in list_applicants:
            # try:
            applicant_additional_county = []
            applicant_court_fees = []
            no_additional_check_run = []
            applicant_name = str(list_applicant.get('first_name')) + ' ' + str(list_applicant.get('last_name', 'NA'))
            order_date = list_applicant.get('created').strftime("%m/%d/%Y")
            client_user_id = list_applicant.get('client_user_id', 'NA')
            client_user = "client_user_name(client_user_id)"
            # client_user = client_user_name(client_user_id)
            additional_check = list_applicant.get('package', dict()).get('additional_checks')
            ssn_id = list_applicant.get('ssn_id')
            if ssn_id:
                ssn_id = 'XXX-XX-{}'.format(ssn_id.split('-')[2])
            package = list_applicant['package']

            applicant_additional_check = get_applicant_additional_check_info(list_applicant, start_date, end_date, additional_check_run_fee)
            list_applicant['package']['additional_checks'] = applicant_additional_check.get('new_additional_check', [])
            additional_check_suborders = applicant_additional_check.get('additional_crim_check').get('suborders')
            additional_drivrec_check_suborders = applicant_additional_check.get('additional_drivinglicense_check').get(
                'suborders')

            if additional_check_suborders:
                # Get court fees for county in additional check run
                additional_check_countys = get_county_fees(additional_check_suborders)
                for county in additional_check_countys:
                    if county:
                        applicant_additional_county.append(county)
                        court_fees = float(county.get('court_fee'))
                        applicant_court_fees.append(court_fees)

            additional_drivrec_fees = []
            if additional_drivrec_check_suborders:
                additional_drivrec_fees = get_mvr_fees(additional_drivrec_check_suborders, additional=True)
                applicant_court_fees.append(sum(additional_drivrec_fees))

            applicant_additional_county_fee = [fee['court_fee'] for fee in applicant_additional_county]

            additional_check_fee = applicant_additional_check.get('applicant_additional_check_run_fee')
            if additional_check:
                no_additional_check_run.extend(
                    [len(applicant_additional_check.get('additional_crim_check').get('suborders', [])),
                     len(applicant_additional_check.get('additional_natcrim_check').get('suborders', [])),
                     len(applicant_additional_check.get('additional_insuranceveri_check').get('suborders', [])),
                     len(applicant_additional_check.get('additional_ssntrace_check').get('suborders', [])),
                     len(applicant_additional_check.get('additional_drivinglicense_check').get('suborders', []))])

            # adding tax value ,state fees
            state_fees = sum(applicant_court_fees)

            # calculating tax value
            if client_state:
                state = mongo.states.find_one({'StateCode': client_state})
                tax = state.get('StateTax') if state else 0
            else:
                tax = 0

            total_check_price = float(additional_check_fee)
            tax_value = total_check_price * float(tax) / 100

            discount = get_applicant_discount(list_applicant, start_date, end_date)
            additional_cost = get_applicant_additional_cost(list_applicant, start_date, end_date)
            total_amount = total_check_price + tax_value + state_fees - discount + additional_cost

            applicant = {'order_date': order_date, 'created_by': client_user,
                         'additional_check_fee': "%.2f" % additional_check_fee, 'package': package,
                         'additional_county': applicant_additional_county,
                         'additional_drivrec_fees': additional_drivrec_fees,
                         'additional_county_fee': "%.2f" % sum(applicant_additional_county_fee),
                         'total_county_fee': "%.2f" % (
                                 sum(applicant_additional_county_fee) + sum(additional_drivrec_fees)),
                         'ssn_id': str(ssn_id), 'tax': float(tax), 'state_fees': "%.2f" % state_fees,
                         'discount': "%.2f" % discount, 'additional_cost': "%.2f" % additional_cost,
                         'name': applicant_name.title(), 'pkg_check_price': "%.2f" % total_check_price,
                         'sum_additional_drivrec_fees': "%.2f" % sum(additional_drivrec_fees),
                         'tax_value': "%.2f" % tax_value, 'additional_applicant': True,
                         'final_amount': "%.2f" % total_amount
                         }
            data['all_state_fees'].append(float(state_fees))
            data['no_additional_check_run'].extend(no_additional_check_run)
            data['all_amount'].append(float(total_amount))
            data['all_discount'].append(float(discount))
            data['all_additional_cost'].append(float(additional_cost))
            data['all_pkgcheck_price'].append(float(total_check_price))
            data['all_tax'].append(float(tax_value))
            data['all_applicant'].append(applicant)

            # except Exception as e:
            #     data['applicant_skipped'].append(
            #         "{} error in applicant  {}".format(e, list_applicant['_id']))
    return data
    # except Exception as e:
    #     raise Exception('%s' %e)


def remove_duplicate_counties_report_result(additional_check_suborders, report_results_suborders, key):
    new_report_results_suborders = []
    if additional_check_suborders and report_results_suborders:
        for y in report_results_suborders:
            county_matched = False
            for x in additional_check_suborders:
                if key == "lrg":
                    if x.get('state').upper() == y.get("inputs").get('state').upper() and x.get(
                            'county').upper() == y.get("inputs").get('county').upper():
                        county_matched = True
                else:
                    if x.get('state').upper() == y.get('state').upper() and x.get('county').upper() == y.get(
                            'county').upper():
                        county_matched = True
            if not county_matched:
                new_report_results_suborders.append(y)
    else:
        new_report_results_suborders = report_results_suborders
    return new_report_results_suborders



def get_missed_additional_check_applicant(applicants, start_date, end_date):
    try:
        list_applicants = []
        for applicant in applicants:
            try:
                additional_checks = applicant.get('package').get('additional_checks', dict())
                append_applicant = False
                drivrec_suborder = []
                ssntrace_suborder = []
                crimcounty_suborder = []
                natcrim_suborder = []
                insuranceveri_suborder = []

                for check, check_suborders in additional_checks.items():
                    for i, suborder in enumerate(check_suborders.get('suborders')):
                        if str(check) == 'drivrec':
                            if suborder.get(check).get('created_at'):
                                if start_date <= suborder.get(check).get('created_at') <= end_date:
                                    drivrec_suborder.append(suborder)
                        else:
                            if suborder.get('created_at'):
                                if start_date <= suborder.get('created_at') <= end_date:
                                    if check == 'crimcounty':
                                        crimcounty_suborder.append(suborder)
                                        append_applicant = True
                                    if check == 'ssntrace':
                                        ssntrace_suborder.append(suborder)
                                        append_applicant = True
                                    if check == 'natcrim':
                                        natcrim_suborder.append(suborder)
                                        append_applicant = True
                                    if check == 'Insuranceveri':
                                        insuranceveri_suborder.append(suborder)
                                        append_applicant = True

                for additional_cost in applicant.get('package', dict()).get('additional_cost', []):
                    if isinstance(additional_cost, dict):
                        if start_date <= additional_cost.get('created_at') <= end_date:
                            append_applicant = True

                for discount_cost in applicant.get('package', dict()).get('discount_cost', dict()):
                    if start_date <= discount_cost.get('created_at') <= end_date:
                        append_applicant = True

                applicants_found = get_applicant_with_on_demand_renewals_previous_month([applicant], start_date,
                                                                                        end_date)
                if applicants_found:
                    append_applicant = False

                additional_checks.get('drivrec', dict())['suborders'] = drivrec_suborder

                additional_checks.get('ssntrace', dict())['suborders'] = ssntrace_suborder

                additional_checks.get('crimcounty', dict())['suborders'] = crimcounty_suborder

                additional_checks.get('natcrim', dict())['suborders'] = natcrim_suborder

                additional_checks.get('Insuranceveri', dict())['suborders'] = insuranceveri_suborder

                if append_applicant:
                    list_applicants.append(applicant)

            except Exception as e:
                LOG.error(
                    "{} error in applicant  {}  ".format(e, applicant['_id']))
                continue
        return list_applicants
    except Exception as e:
        raise Exception('%s' % e)


def remove_duplicate_mvr_record_drivinglicense_cases(additional_drivrec_check_suborders,drivinglicense_cases):
    new_drivinglicense_cases = []
    if additional_drivrec_check_suborders and drivinglicense_cases:
        for case_y in drivinglicense_cases:
            y = case_y.get('inputs')
            dl_record_matched = False
            for x in additional_drivrec_check_suborders:
                x = x.get('drivrec').get('inputs')
                if x.get('licenseState_shortName').upper() == y.get(
                        'dl_state').upper() and x.get(
                        'license_number').upper() == y.get('dl_no').upper():
                    dl_record_matched = True
            if not dl_record_matched:
                new_drivinglicense_cases.append(case_y)
    else:
        new_drivinglicense_cases = drivinglicense_cases

    return new_drivinglicense_cases

