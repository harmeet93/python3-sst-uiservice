import flask
from app import smartscreen_api as api
from flask import request, make_response, jsonify, Response
import json
from app import mongo, LOG
from bson import json_util, ObjectId
from functools import wraps
from app.utils import app_business
import app.admin.business as admin_business
# from app.admin.business import signup_route
from app.jwt_auth import jwt_required, get_current_user, access_authorized
from app.main import JSONOIDEncoder
from app.main import make_response_custom_json_encoder, compare_time_passed
from app.jwt_auth import issue_access_token_for_api_user_id, jwt_required, issue_access_token_for_api_user, get_routes_ui_aliases_for_api_user 
# from weasyprint import HTML
from flask import Flask, render_template
from flask_weasyprint import HTML, render_pdf
import datetime

response_cont = {'response': {'status': True, 'applicants': [{'order_date': '01/30/2020', 'order__date_format': '2020-01', 'created_by': 'client_user_name(client_user_id)', 'package': {'renewals': [], 'additional_checks': {'drivrec': {'suborders': [{'drivrec': {'inputs': {'licenseState_shortName': 'PA', 'license_number': '45645656', 'license_state': 'Pennsylvania'}, 'created_at': datetime.datetime(2020, 1, 31, 2, 19, 27, 67000)}}]}, 'ssntrace': {'suborders': [{'inputs': {'first_name': 'Additional', 'last_name': 'FL', 'middle_name': '', 'dob': '1996-06-12', 'ssn_id': '567-34-6346', 'middle_name_present': False}, 'created_at': datetime.datetime(2020, 1, 31, 1, 50, 19, 904000)}]}, 'natcrim': {'suborders': [{'first_name': 'John', 'last_name': 'Devine', 'middle_name': '', 'dob': '1997-11-13', 'created_at': datetime.datetime(2020, 1, 31, 1, 51, 23, 288000), 'middle_name_exists': False}]}, 'crimcounty': {'suborders': [{'county': 'ALGER', 'created_at': datetime.datetime(2020, 1, 31, 2, 6, 23, 725000), 'state': 'MI'}]}}, 'description': 'Additional + Cost centers', 'title': 'Additional + Cost centers', 'checklist': [{'price': 0, 'code': 'Insuranceveri', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'Insurance Verification'}, {'price': 0, 'code': 'crimcounty', 'options': {'hasOptions': False, 'scopeOptions': {'max_address_history': {'amount': '0'}, 'max_num_searches': {'amount': '0'}}, 'initial_state_override': None}, 'title': 'Criminal County'}, {'price': 0, 'code': 'drivrec', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'Driving Record'}, {'price': 0, 'code': 'drugs_5pan', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': '5 Panel Drug Screening'}, {'price': 0, 'code': 'fedcrim', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'Federal Criminal'}, {'price': 0, 'code': 'gsa_sam', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'GSA/SAM List'}, {'price': 0, 'code': 'gstw', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'Terrorist Watchlist'}, {'price': 0, 'code': 'natcrim', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'National Criminal'}, {'price': 0, 'code': 'ssntrace', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'SSN Trace'}, {'price': 0, 'code': 'statecrim', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'State Criminal'}], 'price': 200, 'renewal_price': 0, 'additional_cost': [{'additional_cost_comment': 'Additional cost added in researcher', 'created_at': datetime.datetime(2020, 1, 31, 1, 48, 21, 47000), 'additional_cost': 60, 'additional_cost_id': 'additional_cost_0'}], 'ExtraDocs': [], 'discount_cost': [{'discount_id': 'discount_1', 'discount': 25.25, 'created_at': datetime.datetime(2020, 1, 31, 4, 7, 38, 171000), 'discount_comment': 'test'}], 'adjudication_matrix_id': None, '_id': '5e14205bc19a04000b46a19d', 'type': 'regular', 'renewable': False}, 'checks': [{'price': 0, 'code': 'Insuranceveri', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'Insurance Verification'}, {'price': 0, 'code': 'crimcounty', 'options': {'hasOptions': False, 'scopeOptions': {'max_address_history': {'amount': '0'}, 'max_num_searches': {'amount': '0'}}, 'initial_state_override': None}, 'title': 'Criminal County'}, {'price': 0, 'code': 'drivrec', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'Driving Record'}, {'price': 0, 'code': 'drugs_5pan', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': '5 Panel Drug Screening'}, {'price': 0, 'code': 'fedcrim', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'Federal Criminal'}, {'price': 0, 'code': 'gsa_sam', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'GSA/SAM List'}, {'price': 0, 'code': 'gstw', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'Terrorist Watchlist'}, {'price': 0, 'code': 'natcrim', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'National Criminal'}, {'price': 0, 'code': 'ssntrace', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'SSN Trace'}, {'price': 0, 'code': 'statecrim', 'options': {'hasOptions': False, 'initial_state_override': None}, 'title': 'State Criminal'}], 'additional_drivrec_fees': [0], 'sum_additional_drivrec_fees': '0.00', 'drivrec_cases_fees': '0.00', 'package_price': '200.00', 'additional_check_fee': '382.00', 'additional_cost': '60.00', 'report_county': [{'county': 'NA', 'state': 'NA', 'court_fee': 0.0}], 'additional_county': [{'county': 'NA', 'state': 'NA', 'court_fee': 0.0}], 'report_county_fee': '0.00', 'additional_county_fee': '0.00', 'total_county_fee': '0.00', 'pkg_check_price': '582.00', 'ssn_id': 'XXX-XX-6346', 'tax': 0.0, 'state_fees': '0.00', 'discount': '25.25', 'tax_value': '0.00', 'name': 'Additional Fl', 'package_name': 'Additional + Cost centers', 'final_amount': '616.75'}], 'total_applicant': 1, 'total_pkgcheck_price': '582.00', 'total_additional_checks_price': '382.00', 'total_pkg_price': '200.00', 'total_discount': '25.25', 'total_additional_cost': '60.00', 'client_physical_address': 'C101 Flat 1/A, Saudagar Gardens, Kiwale Pune  412101', 'total_tax': '0.00', 'invoice_date': '01/01/2020 - 01/31/2020', 'client_name': 'New Company Pvt Ltdd', 'state_fees': '0.00', 'invoice_id': 'JBYHZ1', 'no_additional_check_run': 4, 'total_amount': '616.75'}, 'check_run_fee': {'ssntrace_running_fee': '250.00', 'drivinglicense_running_fee': '7.00', 'natcrim_running_fee': '5.00', 'crimcounty_running_fee': '120.00', 'insuranceveri_running_fee': '50.00'}}


@api.route('/new_<name>.pdf')
def hello_pdf(name):
    # Make a PDF straight from HTML in a string.
    html = render_template('hello.html', response=response_cont)
    return render_pdf(HTML(string=html))

@api.route("/hello", methods=["GET"])
def index(name=None):

    # import os
    # dirname = os.path.dirname(__file__)
    # Make a PDF straight from HTML in a string.
    # html = render_template('template-1.html', name=name)
    # html = render_template('new.html', name=name)
    # pdf = HTML(string=html).write_pdf()
    # if os.path.exists(dirname):
    #     f = open(os.path.join(dirname, 'mypdf.pdf'), 'wb')
    #     f.write(pdf)
    # return render_pdf(HTML(string=html))
    return render_template("hello.html", response=response_cont)


@api.route("/admin/login", methods=["POST"])
def handle_login():
    """handles the admin login
    """
    try:
        admin_user = admin_business.admin_login(request)
        if isinstance(admin_user, (dict)):
            access_token = issue_access_token_for_api_user_id(json_util.dumps(admin_user['api_user_id']))
            response = make_response(jsonify({
                "email": admin_user['email'],
                "first_name": admin_user['first_name'],
                "last_name": admin_user['last_name'],
                "role": "admin",
                "access_token": access_token,
            }))
        else:
            response = make_response(jsonify({ "status": False, "message": admin_user, "activated": True }), 401)    
        return response
    except Exception as e:
        LOG.exception("Error while login admin {}".format(e))
        response = make_response(jsonify({ "status": False, "message": str(e), "activated": True }), 401)
        return response




@api.route("/admin/client", methods=["GET"])
@jwt_required
def get_all_clients():
    """get client list with pagination for the admin
    """
    try:
	    page = int(request.args['page'])
	    results = int(request.args['results'])
	    response = make_response(jsonify(admin_business.get_clients(page, results)))
    except Exception as e:
    	print("error come here", e)
    	LOG.error(e)
    	raise e
    response.headers['Content-Type'] = "application/json"
    return response


@api.route("/v2/admin/client/invoice", methods=['POST'])
@jwt_required
def admin_client_invoice_generate():
    """
        Admin API to generate invoice for a particular month.
    """
    try:
        api_user = get_current_user()
        admin_user_id = api_user.get_admin_user_id()
        json_data = request.get_json(force=True)
        response = admin_business.generate_invoice(json_data, admin_user_id)
        print("final response", response)
        # response = {"status": False, "message": "success"}
        r = Response(json.dumps(response, default=json_util.default ), mimetype="application/json")
    except Exception as e:
        LOG.exception("error while create invoice {}".format(e))
        response = {"status": False, "message": e}
        r = Response(json.dumps(response, default=json_util.default ), mimetype="application/json")

    return r

