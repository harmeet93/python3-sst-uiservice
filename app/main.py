"""Contains the main functions
"""

import random, string, inspect
from itsdangerous import URLSafeTimedSerializer
from app import mongo, LOG, smartscreen_api as api
from json import loads
from re import compile, VERBOSE
from bson import ObjectId
import datetime
import json
from flask import Response

FREE_GEOIP_URL = "http://freegeoip.net/json/{}"
VALID_IP = compile(r"""
\b
(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
\b
""", VERBOSE)

SECRET_KEY = api.config['SECRET_KEY']
SECURITY_PASSWORD_SALT = api.config['SECURITY_PASSWORD_SALT']


class JSONOIDEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, (datetime.datetime, datetime.date)):
            return o.isoformat()
        return json.JSONEncoder.default(self, o)


def make_response_custom_json_encoder(dict_data):
    return Response(response=json.dumps(dict_data, cls=JSONOIDEncoder), content_type="application/json")



def compare_time_passed(collection, document_id, field, period_value, period_units):
    """
    Compares time passed since value stored in collection's field with period set by value and units
    Returns -1 if period didn't passed, 1 if passed and 0 if period is equal to passed time
    """
    obj = mongo[collection].find_one({"_id": ObjectId(document_id)})

    if nested_key_in_obj(obj, field):
        actual_dt = deep_get(obj, field)
        actual_delta = datetime.datetime.now() - actual_dt
    else:
        actual_delta = datetime.timedelta(0)  # case when field does't exists

    diff = getattr(actual_delta, period_units)  # value of difference in selected period units

    if diff == period_value:
        return 0
    elif diff > period_value:
        return 1
    else:
        return -1

def nested_key_in_obj(obj, key):
    """
    Returns True if key (including composite) contained in obj
    """
    if '.' in key:  # composite key e.g. 'post.author.full_name'
        prefix = key.split('.')[0]
        main_part = '.'.join(key.split('.')[1:])

        return nested_key_in_obj(obj[prefix], main_part)
    else:
        return key in obj

