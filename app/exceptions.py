from flask import request
from app import LOG


class ClientAPIV3Exception(Exception):
    """Base-class for all client API v3 exceptions"""


class ApplicantAPIv3Exception(Exception):
    """Base-class for all applicant API v3 exceptions"""


class ApplicantUploadNotFound(ApplicantAPIv3Exception):
    """No uploads found in upload collection"""
    pass


class APIRequestFieldMissing(ApplicantAPIv3Exception):
    """API request fields missing"""
    pass


class ApplicantLookupError(ApplicantAPIv3Exception):
    """Applicant not found"""
    pass


class ResearcherAPIException(Exception):
    pass

class ApplicantAPIException(Exception):
    """Base-class for all applicant API v2 exceptions"""
    

class MissingRequestParam(ClientAPIV3Exception):
    pass



class NoRecordFound(Exception):
    def __init__(self, message, code=200, status=False):
        super().__init__(message)
        self.code = 200
        self.status = False
        LOG.error(message)


class CheckProtectionError(Exception):
    def __init__(self, message, code=400, status=False):
        super().__init__(message)
        self.code = 400
        self.status = False
        LOG.error(message)

class LoginError(Exception):
    def __init__(self, message, code=400, status=False):
        super().__init__(message)
        self.code = 400
        self.status = False
        LOG.error(message)

class ApplicantMissingField(ApplicantAPIException):
    """Raised when some important field not found in applicant document"""
    def __init__(self, message, code=400, status=False):
        super().__init__(message)
        self.code = 400
        self.status = False
        LOG.error(message)
