from settings import ENV
import logging
from logging.handlers import RotatingFileHandler



logging.basicConfig(level="INFO")
LOG = logging.getLogger('API')
LOG.propagate = False

INFO_FORMAT = '[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s'
DEBUG_FORMAT = '[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s'
TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S %z'

file_handler = RotatingFileHandler('log/uiservice.log', 'a', 1 * 1024 * 1024, 10)

if ENV == 'local':
	file_handler.setLevel(logging.DEBUG)
	formatter = logging.Formatter(INFO_FORMAT, TIMESTAMP_FORMAT)
	file_handler.setFormatter(formatter)

if ENV == 'production':
	formatter = logging.Formatter(DEBUG_FORMAT, TIMESTAMP_FORMAT)
	file_handler.setLevel(logging.ERROR)
	file_handler.setFormatter(formatter)

LOG.addHandler(file_handler)
