# SST-UISERVICE - Server Side Components

## Pre-requisites
python 3.8
git
mongodb

# Installation on Ubuntu 18.04

### Install Python instruments
1) Install pip3 pckage manager  ```sudo apt update```   ```sudo apt install python3-pip```
2) Install virtualenv python3 package ```sudo pip3 install virtualenv```

### Start virtualenv and get the project

```
$ virtualenv -p python3.8 venv 
$ source venv/bin/activate
$ git clone <sst-uiservice_repo_url>  
$ cd python3-sst-uiservice
```


## Install all project dependencies:
### Install all project related dependencies

```pip3 install -r requirements.txt```
For installing it's asking password so give this password ```singh@123``` because its installing vetty_utills package.



## Firing up the APIs
Make sure the virtual env is activated and all dependencies installed.

### Mongodump is needed to install mongo on the machine locally

##### Get mongodump into a dump directory

```mongodump --host localhost --port 27017 --db sst_local_database```

##### Extract mongodump

```mongorestore --host localhost --port 27017 --db sst_local_database dump/sst_local_database```

### documentation command
for sphinx documentation make heml command need to run
#### run on root folder
```cd docs/ && make html && cd ../
```


### Run the server 
with the gunicorn
```gunicorn app:smartscreen_api --reload --workers 1 -b 127.0.0.1:8000```













