# Researcher

## This document contains All detail's of researcher API with request and response details

---

#### Login Researcher 


- Endpoint ```admin/researcher/loginresearcher/order/applicant_id```
- Method ```POST```
- Token Required ```No```
- Request params ```form data email and password```

#### Response

Response will be token and researcher details

---

#### Get Details of an Applicant via applicant_id


- Endpoint ```researcher/order/applicant_id```
- Method ```GET```
- Token Required ```Yes```
- Request params ```None```

#### Response


---

