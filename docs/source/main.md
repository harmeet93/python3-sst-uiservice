# Vetty

### Screening Solutions That Move As Fast As You Do

Vetty knows and understands high volume placement. Our technology gives businesses like yours the speed, accuracy, and scalability you need. 

### Why Vetty?

The Vetty Platform
Preposterously easy to use and powerful product built for the throughput and frictionless onboarding.
Make informed decisions effortlessly with a workflow designed that fits the way you work.

---

### System Requirement's

 - Ubuntu 18.04
 - Mongo 4.1
 - Python 3.8
 - Gunicorn 20

---

# SST-UIservices

## Setup

- Install all dependencies with requirements.txt
- Go to ```docs/source``` and then Run Command ```make html```
- Run server command ```gunicorn app:smartscreen_api --reload --workers 1 -b 127.0.0.1:8000```


