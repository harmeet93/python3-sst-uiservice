.. vetty-demo documentation master file, created by
   sphinx-quickstart on Tue Jan 21 11:49:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to vetty-demo's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   main
   researcher


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
